__author__ = 'fabio'

import json
import datetime


class RaceSession(object):
    def __init__(self, data):
        self.__dict__ = data


class Race(object):
    def __init__(self, data):
        self.__raw = data;
        self.race_session = RaceSession(data['raceSession'])
        self.track = Track(data['track'])
        self.ticks = []
        self.cars = []
        for car in data['cars']:
            self.cars.append(Car(car))

    def start(self):
        self.started_at = datetime.datetime.now()
        self.has_started = True
        print("Race started at %s" % self.started_at)

    def end(self):
        self.ended_at = datetime.datetime.now()
        print("Race ended at %s" % self.ended_at)
        #self.dump_to_file()

    def describe(self):
        self.track.describe()

    def tick(self, car_positions, game_id, game_tick):
        self.ticks.append(car_positions)
        return 0.65

    def dump_to_file(self):
        with open('%s_race.json' % self.track.name, 'w') as outfile:
            json.dump(self.__raw, outfile, ensure_ascii=False)

        with open('%s_ticks.json' % self.track.name, 'w') as outfile:
            for tick in self.ticks:
                json.dump(tick, outfile, ensure_ascii=False)
                outfile.write('\n')


class Track(object):
    def __init__(self, data):
        self.id = data['id']
        self.name = data['name']

        self.pieces = []
        for piece in data['pieces']:
            self.pieces.append(TrackPiece.make_piece(piece))

        self.lanes = []
        for lane in data['lanes']:
            self.lanes.append(Lane(lane["index"], lane["distanceFromCenter"]))

    def filter_by_type(self, piece_type):
        for el in self.pieces:
            if isinstance(el, piece_type):
                yield el

    def describe(self):
        print("Track id: %s" % self.id)
        print("Track name: %s" % self.name)
        print("Number of pieces: %s" % len(self.pieces))
        print("Number of straight pieces: %s" % sum(1 for i in self.filter_by_type(StraightPiece)))
        print("Number of bend pieces: %s" % sum(1 for i in self.filter_by_type(BendPiece)))
        print("Number of lanes: %s" % len(self.lanes))


"""
For a Straight the length attribute depicts the length of the piece.
Additionally, there may be a switch attribute that indicates that this piece is a switch piece, i.e. you can switch lanes here.
The bridge attribute indicates that this piece is on a bridge. This attribute is merely a visualization cue and does not affect the actual game.
For a Bend, there's radius that depicts the radius of the circle that would be formed by this kind of pieces.
More specifically, this is the radius at the center line of the track.
The angle attribute is the angle of the circular segment that the piece forms.
For left turns, this is a negative value. For a 45-degree bend to the left, the angle would be -45.
For a 30-degree turn to the right, this would be 30.
"""


class TrackPiece(object):

    @classmethod
    def make_piece(cls, data):
        if 'length' in data:
            return StraightPiece(data['length'], data['switch'] if 'switch' in data else None)
        else:
            return BendPiece(data['radius'], data['angle'])

    # def __repr__(self):
    #     return json.dumps(self)


class StraightPiece(TrackPiece):
    #Constructor
    def __init__ (self, length, switch):
        self.length = length
        self.is_switch = switch


class BendPiece(TrackPiece):
    #Constructor
    def __init__(self, radius, angle):
        self.radius = radius
        self.angle = angle

    def is_left_turn(self):
        return self.angle < 0

    def is_right_turn(self):
        return self.angle > 0


class Car(object):
    def __init__(self, data):
        self.__dict__ = data


class Lane(object):
    def __init__(self, index, distance_from_center):
        self.index = index
        self.distance_from_center = distance_from_center


class CarPositions(object):
    def __init__(self, data):
        self.__dict__ = data