#!/bin/bash
echo "________          ____________   ___________";
echo "___  __ \___________  __ \__  | / /__  ____/";
echo "__  / / /  _ \  _ \  / / /_   |/ /__  __/   ";
echo "_  /_/ //  __/  __/ /_/ /_  /|  / _  /___   ";
echo "/_____/ \___/\___/\____/ /_/ |_/  /_____/   ";
echo "                                            ";

#hosts=( testserver.helloworldopen.com hakkinen.helloworldopen.com senna.helloworldopen.com webber.helloworldopen.com prost.helloworldopen.com )
host=testserver.helloworldopen.com
port=8091
botName=fff
botKey="KP+J+/TjFjFbtg"
#launchers=( "racer3" "noTurboFuzzy" "noTurbo" "fuzzy5" )
launchers=( "brainies" )
tracks=( "keimola" "france" "imola" "england" "usa" "elaeintarha" "suzuka" "pentag" "germany" )
#tracks=( "keimola" )
NOW=$(date +%s)

for l in "${launchers[@]}"
do
	echo "Launching $l"
	for i in "${tracks[@]}"
	do
	   echo "Starting race on track $i"
	   java -cp bot-0.1-SNAPSHOT-jar-with-dependencies.jar done.Main $host $port $botName $botKey $i $l > $i-$NOW.log &
	   echo "Waiting 180 seconds to start next race..."
	   sleep 180
	done
	echo "Waiting until all races are finished!! It might take a while!!"
	wait
done


	