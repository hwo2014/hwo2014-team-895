package done.model;

public interface TrackPiece {

	Boolean isStraight();
	
	Boolean containsSwitch();
	
	void setContainsSwitch(Boolean containsSwitch);
	
	Boolean isBend();
	
	Double length();

	Double length(Lane lane);

	Double distanceUntil(PiecePosition piecePosition);

	void index(int i);

	Integer index();
}
