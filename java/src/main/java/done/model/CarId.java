package done.model;

import done.gson.GsonFactory;

public class CarId {
	
	private String name;
	private String color;
	
	@SuppressWarnings("unused")
	private CarId() {
	}

	public CarId(String name, String color) {
		super();
		this.name = name;
		this.color = color;
	}

	public String name() {
		return name;
	}
	
	public String color() {
		return color;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarId other = (CarId) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}
}