package done.model;

/**
 * Describes the position of a specific car on the track.
 */
public class PiecePosition {

	/**
	 * A pair of lane indices. Usually startLaneIndex and endLaneIndex are
	 * equal, but they do differ when the car is currently switching lane
	 */
	public static class Lane {

		Integer startLaneIndex;
		Integer endLaneIndex;
		
		@SuppressWarnings("unused")
		private Lane() {
			//gson
		}
		
		public Lane(Integer startLaneIndex, Integer endLaneIndex) {
			super();
			this.startLaneIndex = startLaneIndex;
			this.endLaneIndex = endLaneIndex;
		}
		
		public Integer index() {
			return endLaneIndex;
		}

		@Override
		public String toString() {
			return "Lane [startLaneIndex=" + startLaneIndex + ", endLaneIndex="
					+ endLaneIndex + "]";
		}

		public Integer startLaneIndex() {
			return startLaneIndex;
		}
		
		public Integer endLaneIndex() {
			return endLaneIndex;
		}
	}

	/**
	 * Zero based index of the piece the car is on
	 */
	Integer pieceIndex;

	/**
	 * The distance the car's Guide Flag (see above) has traveled from the start
	 * of the piece along the current lane
	 */
	Float inPieceDistance;
	Lane lane;

	/**
	 * The number of laps the car has completed. The number 0 indicates that the
	 * car is on its first lap. The number -1 indicates that it has not yet
	 * crossed the start line to begin it's first lap.
	 */
	Integer lap;

	@SuppressWarnings("unused")
	private PiecePosition() {
	}
	
	public PiecePosition(Integer pieceIndex, Float inPieceDistance, Lane lane,
			Integer lap) {
		super();
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}

	public Integer pieceIndex() {
		return pieceIndex;
	}

	public Float inPieceDistance() {
		return inPieceDistance;
	}

	public Lane lane() {
		return lane;
	}

	public Integer lap() {
		return lap;
	}

	@Override
	public String toString() {
		return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", lane=" + lane + ", lap=" + lap + "]";
	}

}