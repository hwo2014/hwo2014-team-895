package done.model;

import done.gson.GsonFactory;

public class StraightPiece implements TrackPiece {

	Double length;
	Boolean containsSwitch = false;
	private int index;
	
	@Override
	public Boolean isStraight() {
		return true;
	}
	
	@Override
	public Boolean isBend() {
		return false;
	}
	
	@Override
	public Double length() {
		return length;
	}

	@Override
	public Double length(Lane lane) {
		return length();
	}
	
	public Boolean containsSwitch() {
		return containsSwitch;
	}
	
	@Override
	public void setContainsSwitch(Boolean containsSwitch) {
		this.containsSwitch = containsSwitch;
	}

	@Override
	public Double distanceUntil(PiecePosition piecePosition) {
		return length() - piecePosition.inPieceDistance();
	}

	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}
	
	@Override
	public void index(int i) {
		this.index = i;
	}

	@Override
	public Integer index() {
		return index;
	}
	
}
