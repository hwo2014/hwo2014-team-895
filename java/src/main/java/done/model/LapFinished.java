package done.model;

//{"msgType": "lapFinished", "data": {
//	  "car": {
//	    "name": "Schumacher",
//	    "color": "red"
//	  },
//	  "lapTime": {
//	    "lap": 1,
//	    "ticks": 666,
//	    "millis": 6660
//	  },
//	  "raceTime": {
//	    "laps": 1,
//	    "ticks": 666,
//	    "millis": 6660
//	  },
//	  "ranking": {
//	    "overall": 1,
//	    "fastestLap": 1
//	  }
//	}, "gameId": "OIUHGERJWEOI", "gameTick": 300}
public class LapFinished {

	public class Ranking {
		public Integer overall;
		public Integer fastestLap;
	}

	public class LapTime {
		public Integer lap;
		public Integer ticks;
		public Long millis;
	}

	public class RaceTime {
		public Integer laps;
		public Integer ticks;
		public Long millis;
	}

	private CarId car;
	private LapTime lapTime;
	private RaceTime raceTime;
	private Ranking ranking;

	public CarId car() {
		return car;
	}

	public LapTime lapTime() {
		return lapTime;
	}

	public RaceTime raceTime() {
		return raceTime;
	}

	public Ranking ranking() {
		return ranking;
	}

}
