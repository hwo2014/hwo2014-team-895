package done.model;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class Speedometer {

	private final static Logger LOGGER = LoggerFactory.getLogger(Speedometer.class);
	
	private CarId id;
	private Track track;
	private CarPosition lastPosition = null;
	private Float velocity = 0f;
	private List<Float> velocityHistory = Lists.newArrayList();
	private Double distanceTraveled = 0d;
	
	public Speedometer(CarId carId, Track track) {
		this.id = carId;
		this.track = track;
	}

	public void stimulus(CarPosition position) {
		if (lastPosition != null) {
			if (lastPosition.pieceIndex() != position.pieceIndex() ) {
				setVelocity((float) ((track.piece(lastPosition.pieceIndex())
								.length(lane(position)) - lastPosition.inPieceDistance()) + position.inPieceDistance()));
			} else {
				setVelocity(position.inPieceDistance() - lastPosition.inPieceDistance());
			}
		}
		lastPosition = position;
	}
	
	private Lane lane(CarPosition lastPosition) {
		return track.lane(lastPosition);
	}

	public Float velocity() {
		
		return this.velocity;
	}
	
	public Double distanceTraveled() {
		return distanceTraveled;
	}

	public CarId carId() {
		return id;
	}

	private void setVelocity(Float velocity) {
		if (velocity < 0) {
			LOGGER.error("Velocity should NEVER be negative: {}", velocity);
			//throw new IllegalStateException("Velocity should NEVER be negative");
		}
		this.velocityHistory.add(velocity);
		this.velocity = velocity;
	}
	
	public Double avg() {
		return this.velocityHistory
				.stream()
				.mapToDouble(u -> u.doubleValue())
				.average().getAsDouble();
	}
	
	public Double max() {
		return this.velocityHistory
				.stream()
				.mapToDouble(u -> u.doubleValue())
				.max().getAsDouble();
	}
	
	public Double min() {
		return this.velocityHistory
				.stream()
				.mapToDouble(u -> u.doubleValue())
				.min().getAsDouble();
	}
	
}