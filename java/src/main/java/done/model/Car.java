package done.model;

import done.gson.GsonFactory;

public class Car {

	private CarId id;
	private CarDimensions dimensions;
	private Speedometer speedometer;
	private Race parentRace;
	private Boolean switchedLane = null;
	private int previousLaneIndex;
	private int nextLaneIndex;
	private int currentLaneIndex;
	
	public static class CarDimensions {
		public Float length;
		public Float width;
		public Float guideFlagPosition;
		
		@Override
		public String toString() {
			return GsonFactory.gson().toJson(this);
		}
	}

	public CarId id() {
		return id;
	}

	public CarDimensions dimensions() {
		return dimensions;
	}

	public void setSpeedometer(Speedometer speedometer) {
		this.speedometer = speedometer;
	}
	
	public Speedometer getSpeedometer() {
		return this.speedometer;
	}

	public Double distanceToNextBend(CarPosition carPosition) {
		return race().track().distanceUntilNextBend(carPosition);
	}
	
	public Double velocity() {
		return speedometer.velocity().doubleValue(); 
	}

	public Double distanceTraveled(CarPosition init, CarPosition end) {
		return race().distanceTraveled(end) - race().distanceTraveled(init); 
	}
	
	private Race race() {
		return this.parentRace;
	}

	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}

	public void stimulus(CarPosition position) {
		if (speedometer != null)
			speedometer.stimulus(position);
		trackLaneOperations(position);
	}

	private void trackLaneOperations(CarPosition position) {
		int startLaneIndex = position.piecePosition().lane().startLaneIndex();
		if (this.switchedLane == null) {
			switchedLane = false;
		} else {
			if (this.currentLaneIndex == startLaneIndex) {
				switchedLane = false;
			} else {
				switchedLane = true;
			}
		}
		this.currentLaneIndex = startLaneIndex;
	}
	
	public Boolean switchedLane() {
		return switchedLane == null ? false : switchedLane;
	}

	public void race(Race race) {
		this.parentRace = race;
	}
}
