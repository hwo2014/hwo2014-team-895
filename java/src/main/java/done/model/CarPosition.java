package done.model;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

import done.gson.GsonFactory;


public class CarPosition {

	private CarId id;
	
	/**
	 * The angle depicts the car's slip angle. Normally this is zero, but when
	 * you go to a bend fast enough, the car's tail will start to drift.
	 * Naturally, there are limits to how much you can drift without crashing
	 * out of the track.
	 */
	private Float angle;
	
	private PiecePosition piecePosition;
	
	private Integer prevCommandTick;

	@SuppressWarnings("unused")
	private CarPosition() {
		//gson
	}

	public CarPosition(CarId id, Float angle, PiecePosition piecePosition) {
		super();
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}

	public CarId id() {
		return id;
	}

	public Float angle() {
		return angle;
	}

	public PiecePosition piecePosition() {
		return piecePosition;
	}

	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}

	public Float inPieceDistance() {
		return piecePosition().inPieceDistance();
	}

	public Integer pieceIndex() {
		return piecePosition().pieceIndex();
	}
	
	public CarPosition get(Collection<CarPosition> carPositions) {
		Predicate<CarPosition> myPosition = p -> p.id().equals(id);
		Optional<CarPosition> currentPosition = carPositions.stream().filter(myPosition).findFirst();
		return currentPosition.get();
	}

	public boolean isSwitchingLane() {
		return piecePosition().lane().startLaneIndex() != piecePosition().lane().endLaneIndex();
	}
}
