package done.model;


public enum MessageType {
	
	GAME_INIT("gameInit"),
	GAME_START("gameStart"),
	CRASH("crash"),
	GAME_END("gameEnd"),
	FINISH("finish"),
	SPAWN("spawn"),
	LAP_FINISHED("lapFinished"),
	CAR_POSITIONS("carPositions"),
	ERROR("error"),
	JOIN("join"),
	JOINRACE("joinRace"),
	TOURNAMENTEND("tournamentEnd"),
	YOUR_CAR("yourCar"),
	TURBO_AVAILABLE("turboAvailable"),
	TURBOSTART("turboStart"),
	TURBOEND("turboEnd"),
	CREATERACE("createRace"),
	DNF("dnf"),
	UNKNOWN("");
	
	String type;
	
	MessageType(String type) {
		this.type = type;
	}
	
	public String type() {
		return type;
	}
	
	public static MessageType from(String msgType) {
		for (MessageType tt : values()) {
			if (msgType.equals(tt.type())) {
				return tt;
			}
		}
		return UNKNOWN;
	}
}