package done.model;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import done.gson.GsonFactory;

public class BendPiece implements TrackPiece {

	Double radius;
	Double angle;
	Boolean containsSwitch = false;
	private int index ;
	
    public Boolean isLeftTurn() {
    	return angle < 0;
    }

    public Boolean isRightTurn(){
    	return angle > 0;
    }

	@Override
	public Boolean isStraight() {
		return false;
	}
	
	@Override
	public Boolean containsSwitch() {
		return containsSwitch;
	}

	@Override
	public void setContainsSwitch(Boolean containsSwitch) {
		this.containsSwitch = containsSwitch;
	}

	@Override
	public Boolean isBend() {
		return true;
	}

	@Override
	public Double length() {
		return  Math.abs((angle * Math.PI * radius) / 180d);
	}

	@Override
	public Double length(Lane lane) {
		double calcRadius =  isRightTurn() ?
				radius - lane.distanceFromCenter : 
				radius + lane.distanceFromCenter; 
		return Math.abs((angle * Math.PI * calcRadius) / 180d);
	}

	@Override
	public Double distanceUntil(PiecePosition piecePosition) {
		return length() - piecePosition.inPieceDistance();
	}	
	
	@Override
	public String toString() {
		Gson gson = GsonFactory.gson();
		JsonObject tree = (JsonObject) gson.toJsonTree(this);
        tree.addProperty("degreeOfCurvature", degreeOfCurvature());
        tree.addProperty("direction", isLeftTurn() ? "left" : "right" );
        return gson.toJson(tree);
	}

	@Override
	public void index(int i) {
		this.index = i;
	}

	@Override
	public Integer index() {
		return index;
	}

	public boolean sameDirection(BendPiece p) {
		return !(isRightTurn() ^  p.isRightTurn());
	}
	
	public Double degreeOfCurvature() {
		return (1/this.radius) * 100 ;
	}
	
}