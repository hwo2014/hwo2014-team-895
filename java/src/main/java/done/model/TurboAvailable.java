package done.model;

import done.gson.GsonFactory;

//{"msgType": "turboAvailable", "data": {
//	"turboDurationMilliseconds": 500.0,
//	"turboDurationTicks": 30,
//	"turboFactor": 3.0
//}}
public class TurboAvailable {

	private Float turboDurationMilliseconds;
	private Integer turboDurationTicks;
	private Float turboFactor;

	public Float durationMilliseconds() {
		return turboDurationMilliseconds;
	}

	public Integer durationTicks() {
		return turboDurationTicks;
	}

	public Float factor() {
		return turboFactor;
	}

	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}

}
