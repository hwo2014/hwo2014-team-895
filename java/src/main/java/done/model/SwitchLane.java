package done.model;

import com.google.common.base.Preconditions;

public class SwitchLane extends SendMsg {

	private String direction;
	
	public SwitchLane(String direction) {
		Preconditions.checkArgument("Right".equals(direction) || "Left".equals(direction));
		this.direction = direction;
	}

	@Override
	protected Object msgData() {
		return direction;
	}
	
	@Override
	protected String msgType() {
		return "switchLane";
	}

}
