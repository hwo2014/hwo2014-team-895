package done.model;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

import done.gson.GsonFactory;

public class Track {

	private final static Logger LOGGER = LoggerFactory.getLogger(Track.class);
	
	public String id;
	public String name;
	public LinkedList<TrackPiece> pieces;
	public List<Lane> lanes;
	public List<TrackPiece> longestStraighSequence;
	private Integer numberOfStraightPieces = 0;
	private Integer numberOfBendPieces = 0;
	private Double totalStraightLength = 0d;
	private Double totalBendLength = 0d;
	
	private List<TrackSegment> straights = Lists.newArrayList();

	private static final Ordering<TrackSegment> orderByLengthDesc = Ordering.from(new Comparator<TrackSegment>() {
		@Override
		public int compare(TrackSegment o1, TrackSegment o2) {
			return o2.length().compareTo(o1.length());
		}
	});

	private static final Comparator<Lane> LaneComparator = new Comparator<Lane>() {
		@Override
		public int compare(Lane o1, Lane o2) {
			return Integer.compare(o1.distanceFromCenter, o2.distanceFromCenter);
		}
	};
	
	public Track(String id, String name, LinkedList<TrackPiece> pieces,
			List<Lane> lanes) {
		super();
		Preconditions.checkNotNull(id);
		Preconditions.checkNotNull(name);
		Preconditions.checkNotNull(pieces);
		Preconditions.checkNotNull(lanes);
		
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
		this.straights = straights();
		
		for (TrackPiece piece : pieces) {
			if (piece.isStraight()) {
				totalStraightLength += piece.length();
				numberOfStraightPieces++;
			} else {
				totalBendLength += piece.length();
				numberOfBendPieces++;
			}
		}
		
		for (Lane lane : lanes) {
			for (TrackPiece piece : pieces) {
				if (piece.isStraight()) {
					lane.totalStraightLength += piece.length();
				} else {
					lane.totalBendLength += piece.length(lane);
				}
			}
		}
	}

	public void describe() {
		LOGGER.info("Track id: {}",id);
		LOGGER.info("Track name: {}",name);

		LOGGER.info("# straight pieces: {}", numberOfStraightPieces);
		LOGGER.info("# bend pieces: {}", numberOfBendPieces);
		LOGGER.info("# pieces: {}",pieces.size());
		
		LOGGER.info("Total straight length: {}", totalStraightLength);
		LOGGER.info("Total bend length: {}", totalBendLength);
		LOGGER.info("Total length: {}, {}% straight, {}% bend", length(), straightLengthPercentage(), bendLengthPercentage());
		
		for (Lane lane : lanes) {
			LOGGER.info("Lane[{}]: Total straight length: {}", lane.index, lane.totalStraightLength);
			LOGGER.info("Lane[{}]: Total bend length: {}", lane.index, lane.totalBendLength);
			LOGGER.info("Lane[{}]: Total length: {}", lane.index, lane.totalStraightLength + lane.totalBendLength);
		}
		
		LOGGER.info("# straights: {}", straights.size());
		for (TrackSegment seg : straights) {
			LOGGER.info("{start:{}, end:{}, length:{}}", seg.first().index(), seg.last().index(), seg.length());
		}
		
	}
	
	public Double length() {
		return totalBendLength + totalStraightLength;
	}

	public Double straightLengthPercentage() {
		return (totalStraightLength / length()) * 100;
	}
	
	public Double bendLengthPercentage() {
		return (totalBendLength / length()) * 100;
	}
	
	public TrackPiece piece(CarPosition position) {
		return pieces.get(position.pieceIndex());
	}

	public Iterable<TrackPiece> cycleThroughPiecesStartingAfter(Integer pieceIndex) {
		TrackPiece piece = pieces.get(pieceIndex);
		Builder<TrackPiece> builder = ImmutableList.<TrackPiece>builder();
		
		if (piece.equals(pieces.getLast())) {
			builder.addAll(pieces);
		} else if (piece.equals(pieces.getFirst())) {
			builder.addAll(pieces.subList(1, pieces.size()));
			builder.add(pieces.getLast());
		} else {
			builder.addAll(pieces.subList(pieceIndex + 1, pieces.size()));
			builder.addAll(pieces.subList(0, pieceIndex + 1));
		}
		
		return Iterables.cycle(builder.build());
	}
	
	public Iterable<TrackPiece> cycleThroughPiecesStartingAfter(CarPosition position) {
		return cycleThroughPiecesStartingAfter(position.pieceIndex());
	}

	public Integer nextTrackPieceIndex(Integer pieceIndex) {
		TrackPiece piece = pieces.get(pieceIndex);
		if (piece.equals(pieces.getLast())) {
			return 0;
		}
		return pieceIndex+1;
	}
	
	public Integer nextTrackPieceIndex(CarPosition position) {
		return nextTrackPieceIndex(position.pieceIndex());
	}
	
	public TrackPiece nextTrackPiece(CarPosition position) {
		return pieces.get(nextTrackPieceIndex(position.pieceIndex()));
	}
	
	public Integer lastTrackPieceIndex(Integer pieceIndex) {
		if (pieceIndex == 0) {
			return pieces.getLast().index();
		}
		return pieceIndex-1;
	}
	
	public Double distanceUntilNextBend(CarPosition position) {
		Float inPieceDistance  = position.inPieceDistance();
		Integer pieceIndex = position.pieceIndex();
		TrackPiece piece = pieces.get(pieceIndex);
		Double partial = piece.length() - inPieceDistance;
		Iterator<TrackPiece> iterator = cycleThroughPiecesStartingAfter(position).iterator();
		while(iterator.hasNext()) {
			TrackPiece p = iterator.next();
			if (p.isStraight()) {
				partial += p.length(); 	
			} else {
				break;
			}
		}
		return partial;
	}

	public TrackPiece piece(Integer index) {
		return pieces.get(index);
	}

	public Double distanceTraveled(CarPosition position) {
		return position == null 
					? 0d 
					: (position.piecePosition().lap() * length()) + (position.inPieceDistance() + distanceUntil(position.pieceIndex()));
	}

	private Double distanceUntil(Integer pieceIndex) {
		return pieces
				.subList(0, pieceIndex)
				.stream()
				.mapToDouble(TrackPiece::length)
				.sum();
	}

	
	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}

	public Lane lane(CarPosition position) {
		for (Lane lane : lanes) {
			if (lane.index  == position.piecePosition().lane().startLaneIndex ) {
				return lane;
			}
		}
		throw new IllegalStateException();
	}
	
	public boolean isLeftLane(CarPosition position) {
		if (position.piecePosition().lane().startLaneIndex == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isRightLane(CarPosition position) {
		if (position.piecePosition().lane().startLaneIndex == (lanes.size()-1)) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean areAdjacent(TrackSegment firstSegment, TrackSegment secondSegment) {
		if ( nextTrackPiece(firstSegment.last()).equals(secondSegment.first()) ||
				nextTrackPiece(secondSegment.last()).equals(firstSegment.first()) ) {
			return true;
		};
		return false;
	}
	
	public TrackPiece nextTrackPiece(TrackPiece piece) {
		int index = pieces.indexOf(piece);
		return piece(nextTrackPieceIndex(index));
	}

	public List<TrackSegment> segments() {
		List<TrackSegment> allSegments = Lists.newArrayList();
		for (TrackSegment straight : straights()) {
			allSegments.add(straight);
			TrackSegment c = new TrackSegment();
			Iterator<TrackPiece> iterator = cycleThroughPiecesStartingAfter(straight.last().index()).iterator();
			while(iterator.hasNext()) {
				TrackPiece p = iterator.next();
				if (p.isBend()) {
					c.add(p);
				} else {
					break;
				}
			}
			if (c.size() > 0)
				allSegments.add(c);
		}
		return allSegments;
	}
	
	public List<TrackSegment> rawCurves() {
		List<TrackSegment> curves = Lists.newArrayList();
		for (TrackSegment straight : rawStraights()) {
			TrackSegment c = new TrackSegment();
			Iterator<TrackPiece> iterator = cycleThroughPiecesStartingAfter(straight.last().index()).iterator();
			while(iterator.hasNext()) {
				TrackPiece p = iterator.next();
				if (p.isBend()) {
					c.add(p);
				} else {
					break;
				}
			}
			if (c.size() > 0)
				curves.add(c);
		}
		return curves;
	}
	
	public List<TrackSegment> rawStraights() {
		List<TrackSegment> segments = Lists.newArrayList();
		int init=-1;
		int size = pieces.size();
		for (int index = 0; index < size; index++) {
			TrackPiece trackPiece = pieces.get(index);
			if (trackPiece.isStraight()) {
				if(init < 0) {
					init = index;
				}
			} else {
				if (init >= 0) {
					segments.add(new TrackSegment(pieces.subList(init, index)));
					init = -1;
				}
			}
		}
		if (init >= 0) {
			segments.add(new TrackSegment(pieces.subList(init, size)));
		}
		return segments;
	}
	
	/**
	 * @return {@link TrackSegment} list ordered by length DESC (longest first)
	 */
	public List<TrackSegment> straights() {
		if (straights == null || straights.isEmpty()) {
			straights = orderByLengthDesc.sortedCopy(mergeFirstAndLastIfAdjacents(rawStraights()));
		}
		return straights; 
	}
	
	private List<TrackSegment> mergeFirstAndLastIfAdjacents(List<TrackSegment> segments) {
		TrackSegment firstSegment = segments.get(0);
		TrackSegment lastSegment = segments.get(segments.size()-1);
		if(areAdjacent(firstSegment, lastSegment)) {
			List<TrackPiece> segmentPieces = ImmutableList.<TrackPiece>builder()
					.addAll(lastSegment.innerPieces)
					.addAll(firstSegment.innerPieces)
					.build()  ;
			TrackSegment s = new TrackSegment(segmentPieces);
			return ImmutableList.<TrackSegment>builder()
					.add(s)
					.addAll(segments.subList(1, segments.size() - 1))
					.build();
		}
		return segments;
	}

	public class TrackSegment {
		
		List<TrackPiece> innerPieces;
		
		private TrackSegment(List<TrackPiece> pieces) {
			super();
			this.innerPieces = pieces;
		}

		public TrackSegment() {
			innerPieces = Lists.newArrayList();
		}

		public Double length() {
			return this.innerPieces
						.stream()
						.mapToDouble(TrackPiece::length)
						.sum();
		}
		
		public TrackPiece first() {
			return innerPieces.get(0);
		}
		
		public TrackPiece last() {
			return innerPieces.get(innerPieces.size() -1);
		}

		public Integer size() {
			return innerPieces.size();
		}
	
		@Override
		public String toString() {
			return GsonFactory.gson().toJson(this);
		}

		public boolean contains(TrackPiece piece) {
			return piece.index() >= first().index() ||  piece.index() <= last().index()   ;
		}

		public void add(TrackPiece p) {
			innerPieces.add(p);
		}

		public Boolean isBend() {
			return first().isBend();
		}
		
		public Boolean isStraight() {
			return !isBend();
		}
		
		public Double averageCurvature() {
			return isBend() ? innerPieces.stream().map(p -> (BendPiece) p)
					.mapToDouble(i -> i.degreeOfCurvature()).average()
					.getAsDouble() : 0d;
		}

		public TrackSegment concat(TrackSegment s) {
			List<TrackPiece> pieces = Lists.newArrayList(innerPieces);
			pieces.addAll(s.innerPieces);
			return new TrackSegment(pieces);
		}
		
	}

	public TrackSegment longestStraight() {
		return straights.get(0);
	}

	public boolean nextBendIsToTheRight(CarPosition carPosition) {
		return nextBendPiece(carPosition).isRightTurn();
	}
	
	public boolean nextBendIsToTheLeft(CarPosition carPosition) {
		return nextBendPiece(carPosition).isLeftTurn();
	}

	private BendPiece nextBendPiece(CarPosition position) {
		Iterator<TrackPiece> iterator = cycleThroughPiecesStartingAfter(position).iterator();
		while(iterator.hasNext()) {
			TrackPiece p = iterator.next();
			if (p.isBend()) {
				return (BendPiece) p; 	
			}
		}
		throw new IllegalStateException();
	}

	
	public boolean notAtTheFarRight(CarPosition carPosition) {
		return carPosition.piecePosition().lane().index() !=  farRightLane().index ;
	}
	
	public boolean notAtTheFarLeft(CarPosition carPosition) {
		return carPosition.piecePosition().lane().index() !=  farLeftLane().index ;
	}

	private Lane farRightLane() {
		Optional<Lane> lane = lanes.stream().max(LaneComparator);
		return lane.get();
	}
	
	private Lane farLeftLane() {
		Optional<Lane> lane = lanes.stream().min(LaneComparator);
		return lane.get();
	}

	public boolean nextCurveIsToTheRight(CarPosition position) {
		return nextCurveIsToThe(position, true);
	}

	private boolean nextCurveIsToThe(CarPosition position, Boolean right) {
		TrackPiece currentPiece = piece(position);
		if (currentPiece.isBend()) {
			BendPiece currentBendPiece = (BendPiece) currentPiece;
			Iterator<TrackPiece> iterator = cycleThroughPiecesStartingAfter(position).iterator();
			while(iterator.hasNext()) {
				TrackPiece p = iterator.next();
				if (p.isBend()) {
					BendPiece bend = (BendPiece) p;
					if (currentBendPiece.sameDirection(bend)) {
						continue;
					} else {
						return right ? bend.isRightTurn() : bend.isLeftTurn();
					}
				} else {
					return right ? currentBendPiece.isRightTurn() : currentBendPiece.isLeftTurn();
				}
			}
		} else {
			return right ? nextBendIsToTheRight(position) : nextBendIsToTheLeft(position);
		}
		throw new IllegalStateException();
	}

	public boolean nextCurveIsToTheLeft(CarPosition position) {
		return nextCurveIsToThe(position, false);
	}
	
	public List<BendPiece> bendPieces() {
		List<BendPiece> bends = Lists.newArrayList();
		for (TrackPiece piece : pieces) {
			if (piece.isBend()) {
				bends.add((BendPiece) piece);
			} 
		}
		return bends;
	}

	public Double nextBendCurvature(CarPosition position) {
		return nextBendPiece(position).degreeOfCurvature();
	}
	

	public Double nextCurveAvgCurvature(CarPosition position) {
		BendPiece nextBendPiece = nextBendPiece(position);
		List<TrackSegment> allSegments = segments();
		for (TrackSegment trackSegment : allSegments) {
			if (trackSegment.contains(nextBendPiece) && trackSegment.isBend()) {
				return trackSegment.averageCurvature();
			}
		}
		throw new IllegalStateException();
	}
	

	public List<TrackSegment> turboSpots() {
		List<TrackSegment> allSegments = segments();
		List<TrackSegment> turboSpots = Lists.newArrayList();
//		
//		Iterable<TrackSegment> itearable = Iterables.cycle(allSegments);
//		Iterator<TrackSegment> iterator = itearable.iterator();
//		
//		
//		TrackSegment first = iterator.next();
//		TrackSegment tmp = first;
//		while(iterator.hasNext()) {
//			TrackSegment s = iterator.next();
//			if(s == first) {
//				break;
//			} else {
//				if (s.isStraight() || ( s.isBend() && s.averageCurvature() < 0.75d) ) {
//					tmp = tmp.concat(s);
//				} else {
//					turboSpots.add(tmp);
//					tmp = s;
//				}
//			}
//		}
		
		return turboSpots;
	}




}