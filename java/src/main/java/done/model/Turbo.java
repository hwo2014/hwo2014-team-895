package done.model;

public class Turbo extends SendMsg {

	String message;
	
	public Turbo(String message) {
		super();
		this.message = message;
	}

	@Override
	protected Object msgData() {
		return message;
	}
	
	@Override
	protected String msgType() {
		return "turbo";
	}

}
