package done.model;

import done.gson.GsonFactory;

public class GameInit {

	private Race race;

	public Race race() {
		return race;
	}
	
	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}
}
