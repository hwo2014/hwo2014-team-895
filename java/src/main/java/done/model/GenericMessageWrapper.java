package done.model;

public class GenericMessageWrapper<T> {

	String msgType;
    String gameId;
    
	/**
	 * The gameTick attribute indicates the current Server Tick, which is an
	 * increasing integer.
	 */
    Integer gameTick;
    
    T data;
    
    public GenericMessageWrapper(String msgType, String gameId,
    		Integer gameTick, T data) {
    	super();
    	this.msgType = msgType;
    	this.gameId = gameId;
    	this.gameTick = gameTick;
    	this.data = data;
    }
    
	public MessageType type() {
		return MessageType.from(msgType);
	}

	public String gameId() {
		return gameId;
	}

	public Integer gameTick() {
		return gameTick;
	}
	
	public Boolean hasTick() {
		return gameTick != null;
	}
	

	public T data() {
		return data;
	}

	@Override
	public String toString() {
		return "GenericMessageWrapper [msgType=" + msgType + ", gameId="
				+ gameId + ", gameTick=" + gameTick + ", data=" + data + "]";
	}
    
}