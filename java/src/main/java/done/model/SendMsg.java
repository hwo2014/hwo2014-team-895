package done.model;

import com.google.gson.Gson;

public abstract class SendMsg {
	
    private Integer gameTick;

	public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
    protected Integer gameTick() {
    	return gameTick;
    }
    
    public void tick( Integer gameTick) {
    	this.gameTick = gameTick;
    }

}
