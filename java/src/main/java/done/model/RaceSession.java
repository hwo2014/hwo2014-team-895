package done.model;

import done.gson.GsonFactory;

public class RaceSession {

    private Integer laps;
    private Long maxLapTimeMs;
    private Boolean quickRace;
    private Long durationMs;
    
    public Boolean isQualifying() {
    	return durationMs != null;
    }
    
    public Boolean isRace() {
    	return !isQualifying();
    }

	public Integer laps() {
		return laps;
	}

	public Long maxLapTimeMs() {
		return maxLapTimeMs;
	}

	public Boolean quickRace() {
		return quickRace;
	}

	public Long durationMs() {
		return durationMs;
	}
	
	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}
}