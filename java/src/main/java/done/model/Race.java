package done.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.gson.GsonFactory;

public class Race {

	private final static Logger LOGGER = LoggerFactory.getLogger(Race.class);
	
	public Track track;
	public List<Car> cars;
	public Map<CarId,Car> carsMap = new HashMap<CarId, Car>();
	public RaceSession raceSession;
	
	public void describe() {
		track.describe();
		
		if(isQualifying()) {
			LOGGER.info("Qualifying Race. {}", raceSession);
		} else {
			LOGGER.info("Race total lenght: {}", length());
			LOGGER.info("# cars: {}", cars.size());
		}
	}
	
	public void setup() {
		for (Car car : cars) {
			car.setSpeedometer(new Speedometer(car.id(), track()));
			car.race(this);
			carsMap.put(car.id(), car);
		}
	}
	
	private boolean isQualifying() {
		return raceSession.isQualifying();
	}

	/**
	 * Returns the race full length: track length times the number of laps.
	 * @return
	 */
	public Double length() {
		if (isQualifying()) {
			LOGGER.warn("Qualifying races are measured by time. Returning the track length.");
			return trackLength();
		}
		return trackLength() * laps();
	}

	public Double trackLength() {
		return track.length();
	}
	
	public Car car(CarId id) {
		for (Car car : cars) {
			if (car.id().equals(id)) {
				return car;
			}
		}
		throw new IllegalArgumentException();
	}
	
	
	public Double remaining(CarPosition position) {
		//FIXME
		return trackLength() * laps();
	}

	public Track track() {
		return track;
	}
	
	public Integer laps() {
		return raceSession.laps();
	}

	public Double distanceTraveled(CarPosition position) {
		return track().distanceTraveled(position);
	}
	
	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}

	public void onCarPositons(List<CarPosition> carPositions) {
		for (Car car : cars) {
			CarPosition carPosition = carPosition(carPositions, car.id());
			car.stimulus(carPosition);
		}
	}
	
	private CarPosition carPosition(Collection<CarPosition> carPositions, CarId id) {
		Predicate<CarPosition> myPosition = p -> p.id().equals(id);
		Optional<CarPosition> currentPosition = carPositions.stream().filter(myPosition).findFirst();
		return currentPosition.get();
	}
	
}