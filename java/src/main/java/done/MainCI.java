package done;

import static done.bot.BotFactory.multiBrain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import done.bot.FancySwitchLaneStrategy;
import done.bot.NoTurboStrategy;
import done.bot.RacerBrain7;

public class MainCI {

	private final static Logger LOGGER = LoggerFactory.getLogger(MainCI.class);
	
	public static void main(String... args) throws Exception {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];
		
		LOGGER.info("Bot: "+RacerBrain7.class.getSimpleName());
		MDC.put("botName", botName);
		multiBrain(host, port, botName, botKey, new RacerBrain7(new NoTurboStrategy(), new FancySwitchLaneStrategy()))
			.quickRace();
	}
}