package done;

import done.model.Tracks;

public class MultipleRacesMain {
	
	public static void main(String... args) throws Exception {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		MultipleRacesLauncher l = new MultipleRacesLauncher();
		for (Tracks track : Tracks.values()) {
			l.addRaceLauncher(
				new RaceLauncher(
						track.name() 
		//					,noob(host, port, botName, botKey)
		//					,noob(host, port, "noob1", botKey)
		//					,noob(host, port, "noob2", botKey)
		//					,noob(host, port, "noob3", botKey)
//						,brainy(host, port, "simple", botKey, new SimpleBrain())
//						,brainy(host, port, "fuzzy2", botKey, new FuzzyBotBrain2())
//						,brainy(host, port, "fuzzy3", botKey, new FuzzyBotBrain3())
//						,brainy(host, port, "fuzzy4", botKey, new FuzzyBotBrain4())
//						,brainy(host, port, "fuzzy", botKey, new FuzzyBotBrain())
					)
			);
		}
		l.launchAll();
	}

}