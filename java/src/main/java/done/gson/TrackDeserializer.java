package done.gson;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import done.model.Lane;
import done.model.Track;
import done.model.TrackPiece;

class TrackDeserializer implements JsonDeserializer<Track> {
	
	private final static Type trackPieceListType = new TypeToken<LinkedList<TrackPiece>>(){}.getType();
	private final static Type trackLaneListType = new TypeToken<List<Lane>>(){}.getType();
	
	private Gson gson;

	public TrackDeserializer() {
		super();
		this.gson = new GsonBuilder()
			.registerTypeAdapter(TrackPiece.class, new TrackPieceDeserializer())
			.create();		
	}

	public Track deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
	      throws JsonParseException {
		  JsonObject obj = json.getAsJsonObject();
		  String id = obj.get("id").getAsString();
		  String name =  obj.get("name").getAsString();
		  JsonArray jsonPieces = obj.get("pieces").getAsJsonArray();
		  JsonArray jsonLanes = obj.get("lanes").getAsJsonArray();
		  LinkedList<TrackPiece> pieces = gson.fromJson(jsonPieces, trackPieceListType);
		  for (int i = 0; i < pieces.size(); i++) {
			pieces.get(i).index(i);
		  }
		  List<Lane> lanes = gson.fromJson(jsonLanes, trackLaneListType) ;
		  return new Track(id, name, pieces, lanes) ;
	  }
}