package done.gson;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import done.model.BendPiece;
import done.model.StraightPiece;
import done.model.TrackPiece;

public class TrackPieceDeserializer implements JsonDeserializer<TrackPiece> {
	
	private Gson gson;

	public TrackPieceDeserializer() {
		super();
		gson = new Gson();
	}
	
	public TrackPieceDeserializer(Gson gson) {
		super();
		this.gson = gson;
	}

	public TrackPiece deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
	      throws JsonParseException {
		  JsonObject obj = json.getAsJsonObject();
		  TrackPiece piece;
		  if (obj.has("length")) {
			  piece = this.gson.fromJson(json, StraightPiece.class);
		  } else {
			  piece = this.gson.fromJson(json, BendPiece.class);
		  }
		  if (obj.has("switch")) {
			  piece.setContainsSwitch(true);
		  } else {
			  piece.setContainsSwitch(false);
		  }
		  return piece;
	  }
}