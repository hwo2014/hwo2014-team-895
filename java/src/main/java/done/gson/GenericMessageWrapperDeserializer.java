package done.gson;

import java.lang.reflect.Type;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import done.engine.CreateRace;
import done.model.CarId;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.LapFinished;
import done.model.MessageType;
import done.model.Track;
import done.model.TurboAvailable;

public class GenericMessageWrapperDeserializer implements JsonDeserializer<GenericMessageWrapper<?>> {

	private final static Logger LOGGER = LoggerFactory.getLogger(GenericMessageWrapperDeserializer.class);
	
	private Gson gson;
	
	public GenericMessageWrapperDeserializer(GsonBuilder gsonBuilder) {
		super();
		gsonBuilder.registerTypeAdapter(Track.class, new TrackDeserializer());
		this.gson = gsonBuilder.create();
	}

	@Override
	public GenericMessageWrapper<?> deserialize(JsonElement element,
			Type type, JsonDeserializationContext ctx)
			throws JsonParseException {
		
		GenericMessageWrapper<?> msg = null;
		
		if (element.isJsonObject()) {
            JsonObject obj = element.getAsJsonObject();
            String msgType = obj.get("msgType").getAsString();
            LOGGER.debug("Deserializing MsgType: " + msgType);
            String gameId = obj.has("gameId") ? obj.get("gameId").getAsString() : null;
            Integer gameTick = obj.has("gameTick") ? obj.get("gameTick").getAsInt() : null;

            switch (MessageType.from(msgType)) {
				case GAME_INIT:
					JsonObject data = obj.getAsJsonObject("data");
					msg = create(msgType, gameId, gameTick, data, GameInit.class);
					break;
				case GAME_START:
					//{"msgType": "gameStart", "data": null}
					msg = create(msgType, gameId, gameTick, null, Void.class);
					break;
				case CRASH:
					data = obj.getAsJsonObject("data");
					msg = create( msgType, gameId, gameTick, data, CarId.class);
					break;
				case GAME_END:
					data = obj.getAsJsonObject("data");
					msg = create( msgType, gameId, gameTick, data, Object.class);
					break;
				case SPAWN:
					data = obj.getAsJsonObject("data");
					msg = create( msgType, gameId, gameTick, data, CarId.class);
					break;
				case LAP_FINISHED:
					data = obj.getAsJsonObject("data");
					msg = create( msgType, gameId, gameTick, data, LapFinished.class);
					break;
				case CAR_POSITIONS:
					JsonArray array = obj.getAsJsonArray("data");
					List<CarPosition> carPositions = gson.fromJson(array, GsonFactory.carPositionListType) ;
					msg = new GenericMessageWrapper<List<CarPosition>>(msgType, gameId, gameTick, carPositions);
					break;
				case ERROR:
					JsonPrimitive errorMessage = obj.getAsJsonPrimitive("data");
					LOGGER.warn(errorMessage.getAsString());
					msg = create( msgType, gameId, gameTick, errorMessage, String.class);
					break;
				case JOIN:
					msg = create(msgType, gameId, gameTick, null, Void.class);
					break;
				case YOUR_CAR:
					data = obj.getAsJsonObject("data");
					msg = create( msgType, gameId, gameTick, data, CarId.class);
					break;						
				case FINISH:
					msg = create( msgType, gameId, gameTick, null, CarId.class);
					break;
				case TOURNAMENTEND:
					msg = create( msgType, gameId, gameTick, null, Void.class);
					break;
				case JOINRACE:
					msg = create(msgType, gameId, gameTick, null, Void.class);
					break;
				case TURBO_AVAILABLE:
					data = obj.getAsJsonObject("data");
					msg = create(msgType, gameId, gameTick, data, TurboAvailable.class);
					break;
				case TURBOSTART:
					data = obj.getAsJsonObject("data");
					msg = create(msgType, gameId, gameTick, data, CarId.class);
					break;
				case TURBOEND:
					data = obj.getAsJsonObject("data");
					msg = create(msgType, gameId, gameTick, data, CarId.class);
					break;
				case CREATERACE:
					data = obj.getAsJsonObject("data");
					msg = create(msgType, gameId, gameTick, data, CreateRace.class);
					break;
				default:
					JsonElement jsonData = obj.get("data");
					if ((jsonData!=null) && !(jsonData instanceof JsonNull)) {
					    data = (JsonObject) jsonData;
					} else {
						data = null;
					}
					msg = create(msgType, gameId, gameTick, data, Object.class);
					LOGGER.warn("Unknown message type: {} \n {}", msgType, obj.toString());
					break;
				}
		}
		return msg;
	}

	private <E> GenericMessageWrapper<?> create(String msgType, String gameId,
			Integer gameTick, JsonElement jsonData, Class<E> clazz) {
		GenericMessageWrapper<?> msg;
		E data = jsonData != null ? gson.fromJson(jsonData, clazz) : null;
		msg = new GenericMessageWrapper<E>( msgType, gameId, gameTick, data);
		return msg;
	}	
}