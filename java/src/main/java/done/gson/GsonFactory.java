package done.gson;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import done.model.CarPosition;
import done.model.GenericMessageWrapper;

public class GsonFactory {

	public static final Type genericMessageWrapperType = new TypeToken<GenericMessageWrapper<?>>() {}.getType();
	public static final Type carPositionListType = new TypeToken<List<CarPosition>>(){}.getType();
	
	private static final Gson GSON = new GsonBuilder()
		.registerTypeAdapter(genericMessageWrapperType, new GenericMessageWrapperDeserializer(new GsonBuilder()))
		.create();
	
	public static Gson gson() {
		return GSON;
	}
	
}
