package done;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import done.bot.Bot;
import done.engine.FileDumper;

public class RaceLauncher {

	private final static Logger LOGGER = LoggerFactory.getLogger(RaceLauncher.class);
	
	private String trackName;
	private String password = "pwd";
	private Bot[] bots;
	private ExecutorService taskExecutor;
	private Boolean shouldDumpRaceTofile = false;
	
	public RaceLauncher(String trackName, Bot... bots) {
		super();
		this.trackName = trackName;
		this.bots = bots;
		taskExecutor = Executors.newFixedThreadPool(bots.length);
	}

	public RaceLauncher(String trackName, String pwd, Bot... bots) {
		this (trackName, bots);
		this.password = pwd;
	}
	
	public RaceLauncher dumpRaceToFile() {
		shouldDumpRaceTofile = true;
		return this;
	}
	
	void launch() throws InterruptedException {
		
		final int numberOfBots = bots.length;
		LOGGER.info("Launching race for {} bot(s) on track {}." , numberOfBots, trackName);
				
		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				Bot bot = bot(0);
				LOGGER.info("Bot {} creating race.", bot.name());
				bot.createRace(trackName, password, numberOfBots);
			}
		});
		
		final long watingTime = 1000l * (numberOfBots - 1);
		LOGGER.info("Waiting for {} ms", watingTime);
		Thread.sleep(watingTime);
		
		for (int i = 1; i < numberOfBots; i++) {
			final int index = i;
			
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					Bot bot = bot(index);
					LOGGER.info("Bot {} is joining race.", bot.name());
					bot.joinRace(trackName, password, numberOfBots);
				}
			});
			
			LOGGER.info("Waiting for {} ms", watingTime);
			Thread.sleep(watingTime);
		}
		
		taskExecutor.shutdown();
		taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
	}

	private Bot bot(int index) {
		Bot bot = bots[index];
		
		if (shouldDumpRaceTofile) {
			try {
				bot.raceEngine().addRaceEventDumper(new FileDumper(String.format("%s_%s", bot.name(), trackName)));
			} catch (IOException e) {
				LOGGER.warn(e.getMessage(), e);
			}
		}
		MDC.put("botName", bot.name());
		return bot;
	}
	
}