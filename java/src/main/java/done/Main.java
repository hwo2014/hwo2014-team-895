package done;

import static done.bot.BotFactory.brainy;
import static done.bot.BotFactory.brainyTurbo;
import static done.bot.BotFactory.multiBrain;
import static done.bot.BotFactory.noob;
import static done.bot.BotFactory.random;

import java.io.IOException;
import java.net.UnknownHostException;

import done.bot.DistanceToBendTurboStrategy;
import done.bot.FancySwitchLaneStrategy;
import done.bot.FuzzyBotBrain4;
import done.bot.FuzzyBotBrain5;
import done.bot.FuzzyBotBrain6;
import done.bot.FuzzyBotBrain7;
import done.bot.FuzzyBotBrain71;
import done.bot.LongestStraightTurboStrategy;
import done.bot.NoTurboStrategy;
import done.bot.RacerBrain;
import done.bot.RacerBrain2;
import done.bot.RacerBrain3;
import done.bot.RacerBrain7;
import done.bot.RacerBrain71;
import done.bot.SimpleSwitchLaneStrategy;
import done.bot.TwoLongestStraightTurboStrategy;

public class Main {
	
	private static final FancySwitchLaneStrategy SWITCH_LANE_STRATEGY = new FancySwitchLaneStrategy();

	public static void main(String... args) throws Exception {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		String trackName = "suzuka";
		if (args.length > 4) {
			trackName = args[4];
		}
		
		String launcherName = "brainies";
		if (args.length > 5) {
			launcherName = args[5];
		}
		
		switch (launcherName) {
			case "racer3":
				racer3Launcher(host, port, botKey, trackName).launch();
				break;
			case "noTurboFuzzy":
				noTurboFuzzyRaceLauncher(host, port, botKey, trackName).launch();
				break;
			case "brainies":
				brainiesRaceLauncher(host, port, botKey, trackName).launch();
				break;
			case "noTurbo":
				noTurboRaceLauncher(host, port, botKey, trackName).launch();
				break;
			case "pita":
				pitaRaceLauncher(host, port, botKey, trackName).launch();
				break;
			case "fuzzy5":
				fuzzy5RaceLauncher(host, port, botKey, trackName).launch();
				break;
			case "fuzzy7":
				noTurboFuzzyRaceLauncher7(host, port, botKey, trackName).launch();
				break;
			default:
				noTurboFuzzyRaceLauncher(host, port, botKey, trackName).launch();
				break;
		}
	}
	
	public static RaceLauncher racer3Launcher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				, brainyTurbo(host, port, "r3F", botKey, new RacerBrain3(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "r3DF", botKey, new RacerBrain3(new DistanceToBendTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "r3LS", botKey, new RacerBrain3(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
				, brainyTurbo(host, port, "r3S", botKey, new RacerBrain3(new NoTurboStrategy(), new SimpleSwitchLaneStrategy()))
			)
			.dumpRaceToFile();
		return launcher;
	}

	public static RaceLauncher fuzzy5RaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				, brainyTurbo(host, port, "f5ntF", botKey, new FuzzyBotBrain5(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f5dbF", botKey, new FuzzyBotBrain5(new DistanceToBendTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f5lsF", botKey, new FuzzyBotBrain5(new LongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
			)
			.dumpRaceToFile();
		return launcher;
	}

	public static RaceLauncher noTurboFuzzyRaceLauncher7(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				, brainyTurbo(host, port, "f71ntF", botKey, new FuzzyBotBrain71(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f7ntF", botKey, new FuzzyBotBrain7(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "r71LSF", botKey, new RacerBrain71(new LongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "r7ntF", botKey, new RacerBrain7(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "r71ntF", botKey, new RacerBrain71(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
			)
			.dumpRaceToFile();
		return launcher;
	}
	
	
	public static RaceLauncher noTurboFuzzyRaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				, brainyTurbo(host, port, "f4ntF", botKey, new FuzzyBotBrain4(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f6ntF", botKey, new FuzzyBotBrain6(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f7ntF", botKey, new FuzzyBotBrain7(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f5ntF", botKey, new FuzzyBotBrain5(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
			)
			.dumpRaceToFile();
		return launcher;
	}

	public static RaceLauncher noTurboRaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				, brainyTurbo(host, port, "r3ntF", botKey, new RacerBrain3(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f5ntF", botKey, new FuzzyBotBrain5(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "f6ntF", botKey, new FuzzyBotBrain6(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "r2ntF", botKey, new RacerBrain2(new NoTurboStrategy(), SWITCH_LANE_STRATEGY))
			)
			.dumpRaceToFile();
		return launcher;
	}
	
	public static RaceLauncher brainiesRaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				, brainyTurbo(host, port, "racer2", botKey, new RacerBrain2(new TwoLongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "racer7", botKey, new RacerBrain7(new TwoLongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
				, brainyTurbo(host, port, "racer3", botKey, new RacerBrain3(new TwoLongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
				, multiBrain(host, port, "multi", botKey, new FuzzyBotBrain6(new TwoLongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
			)
			.dumpRaceToFile();
		return launcher;
	}

	public static RaceLauncher brainies2RaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				,brainy(host, port, "fuzzy5", botKey, new FuzzyBotBrain5(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
				,brainyTurbo(host, port, "racer2", botKey, new RacerBrain2(new LongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
				,noob(host, port, "noob025", botKey, 0.25d, false, true)
				,noob(host, port, "noob035", botKey, 0.35d, false, false)
			)
			.dumpRaceToFile();
		return launcher;
	}

	public static RaceLauncher funkyRaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				,random(host, port, "don-rnd", botKey)
				,noob(host, port, "noob-065", botKey, 0.5d)
				,brainy(host, port, "fuzzy4", botKey, new FuzzyBotBrain4(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
				,brainy(host, port, "racer", botKey, new RacerBrain(new LongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
			)
			.dumpRaceToFile();
		return launcher;
	}
	
	public static RaceLauncher whackyRaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				,random(host, port, "don-rnd", botKey)
				,noob(host, port, "noob-065", botKey, 0.65d)
			    ,brainyTurbo(host, port, "racer", botKey, new RacerBrain(new LongestStraightTurboStrategy(), SWITCH_LANE_STRATEGY))
//				,brainy(host, port, "fuzzy2", botKey, new FuzzyBotBrain2())
//				,brainy(host, port, "fuzzy", botKey, new FuzzyBotBrain())
			)
			.dumpRaceToFile();
		return launcher;
	}

	public static RaceLauncher pitaRaceLauncher(String host, int port, String botKey,
			String trackName) throws UnknownHostException, IOException {
		RaceLauncher launcher = new RaceLauncher(
				trackName 
				,random(host, port, "don-rnd", botKey)
				,noob(host, port, "noob-065", botKey, 0.65d)
			    ,brainyTurbo(host, port, "racer", botKey, new RacerBrain(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
			    ,brainyTurbo(host, port, "racer2", botKey, new RacerBrain2(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
				,brainy(host, port, "fuzzy5", botKey, new FuzzyBotBrain5(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
				,brainy(host, port, "fuzzy4", botKey, new FuzzyBotBrain4(new LongestStraightTurboStrategy(), new SimpleSwitchLaneStrategy()))
			)
			.dumpRaceToFile();
		return launcher;
	}

}