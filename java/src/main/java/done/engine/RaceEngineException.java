package done.engine;

public class RaceEngineException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RaceEngineException(Exception e) {
		super(e);
	}

	public RaceEngineException(String message) {
		super(message);
	}

}
