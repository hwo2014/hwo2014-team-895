package done.engine;

import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.common.base.Charsets;

import done.model.SendMsg;

public class JsonFileRaceEngine extends AbstractRaceEngine   {

	private RaceEventPublisher eventPublisher;
	
	public JsonFileRaceEngine(String fileName, RaceEventPublisher eventPublisher) {
		super();

		InputStream is = getClass().getResourceAsStream(fileName);
		if (is == null) {
			throw new RaceEngineException("File not found in classpath " + fileName);
		}
		
		reader = new InputStreamReader(is, Charsets.UTF_8);
		this.eventPublisher = eventPublisher;
	}

	@Override
	void send(SendMsg msg) {
		eventPublisher.publish(msg);
	}


}
