package done.engine;

import done.model.SendMsg;

public interface RaceEventPublisher {

	void publish(SendMsg msg);


}