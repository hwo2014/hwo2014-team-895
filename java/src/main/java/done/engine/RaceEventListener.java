package done.engine;

import java.util.List;

import done.model.CarId;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.LapFinished;
import done.model.SendMsg;
import done.model.TurboAvailable;

public interface RaceEventListener {

	/**
	 * Race server acknowledge a quick race join request
	 */
	void onJoin();
	
	void onJoinRace();
	
	void onCreateRace();
	
	/**
	 * This message allows the bot to identify its own car on the track.
	 * Basically it's the color that matters.
	 */
	void onYourCar(GenericMessageWrapper<CarId> carId);
	
	/**
	 * The gameInit message describes the racing track, the current racing
	 * session and the cars on track. The track is described as an array of
	 * pieces that may be either Straights or Bends.
	 */
	void onGameInit(GenericMessageWrapper<GameInit> msg);
	
	/**
	 * The race has started!
	 * @param msg 
	 */
	void onGameStart(GenericMessageWrapper<Void> msg);
	
	/**
	 * The carPositions message describes the position of each car on the track.
	 * @param carPositionstMsg
	 */
	void onCarPositions(GenericMessageWrapper<List<CarPosition>> carPositionstMsg);
	
	/**
	 * Server sends crash message when a car gets out of track.
	 * @param carId
	 */
	void onCrash(GenericMessageWrapper<CarId> carId);
	
	/**
	 * Shortly after the crash, server sends spawn message as the car is restored on the track.
	 * @param carId
	 */
	void onSpawn(GenericMessageWrapper<CarId> carId);
	
	/**
	 * Server sends lapFinished message when a car completes a lap.
	 */
	void onLapFinished(GenericMessageWrapper<LapFinished> msg);
	
	/**
	 * Server sends dnf message when a car is disqualified. Reason is typically
	 * a failure to comply with the bot protocol or that the bot has
	 * disconnected.
	 */
	void onDnf(GenericMessageWrapper<Object> carId);
	
	/**
	 * Server sends finish message when a car finishes the race.
	 * @param carId
	 */
	void onFinish(GenericMessageWrapper<CarId> carId);
	
	/**
	 * When a tournament is over
	 * @param sendMsgHistory 
	 * @param throttleHistory 
	 */
	void onTournamentEnd(List<Double> throttleHistory, List<SendMsg> sendMsgHistory);
	
	/**
	 * When the game is over, the server sends the gameEnd messages that
	 * contains race results.
	 */
	void onGameEnd(GenericMessageWrapper<Object> carId);
	
	/**
	 * When an error occurred
	 * @param error
	 */
	void onError(GenericMessageWrapper<String> error);
	
	/**
	 * The rumor says the server may also send other messages. Expect the
	 * unexpected!
	 * 
	 * @param unknown
	 */
	void onUnknown(GenericMessageWrapper<Object> unknown);

	/**
	 * Turbo multiplies the power of your car's engine for a short period. The
	 * server indicates the availability of turbo using the turboAvailable
	 * message.
	 * @param msg
	 */
	void onTurboAvailable(GenericMessageWrapper<TurboAvailable> msg);
	
	
	void onTurboStart(GenericMessageWrapper<CarId> msg);
	
	
	void onTurboEnd(GenericMessageWrapper<CarId> msg);
	
	
	void onTurboActivationRequested();
	
	void onSwitchLeftRequested();
	
	void onSwitchRightRequested();
}
