package done.engine;

import java.util.List;

import done.model.TurboAvailable;


public interface RaceEngine {

	void addRaceEventListener(RaceEventListener raceEventListener);

	void join(String botName, String key);
	
	void throttle(Double value);
	
	void ping();
	
	
	/**
	 * The switch has to be done BEFORE the switchable piece. Doing it at the
	 * piece will postpone the switch to the following switchable piece.
	 */
	void switchRight();

	/**
	 * The switch has to be done BEFORE the switchable piece. Doing it at the
	 * piece will postpone the switch to the following switchable piece.
	 */
	void switchLeft();
	
	/**
	 * Once you turn it on, you can't turn it off. It'll automatically expire in
	 * 30 ticks (or what ever the number was in the {@link TurboAvailable} message).
	 * 
	 * @param message
	 */
	void turbo(String message);

	void createRace(String name, String key, Integer carCount, String trackName, String password);
	
	void joinRace(String name, String key, Integer carCount);
	
	void joinRace(String name, String key, Integer carCount, String trackName, String password);

	void run();
	
	void addRaceEventDumper(RaceEventDumper raceEventDumper);

	Double lastThrottle();

	List<Double> throttleHistory();

}
