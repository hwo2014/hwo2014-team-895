package done.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;

import done.model.SendMsg;

public class SocketRaceEngine extends AbstractRaceEngine {

	final static Logger LOGGER = LoggerFactory.getLogger(SocketRaceEngine.class);
	private Socket socket;
	private PrintWriter writer;
	
	public SocketRaceEngine(String host, Integer port) throws UnknownHostException, IOException {
		super();
		socket = new Socket(host, port);
		writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), Charsets.UTF_8));
		reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), Charsets.UTF_8));
	}

	@Override
	void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}

}
