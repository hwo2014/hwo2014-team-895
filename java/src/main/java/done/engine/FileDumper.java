package done.engine;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileDumper implements RaceEventDumper {

	private PrintWriter dumper;

	public FileDumper(String fileName) throws IOException {
		super();
		this.dumper = new PrintWriter(new File(fileName + "_dump_"
				+ System.currentTimeMillis() + ".json"));
	}

	@Override
	public void println(String event) {
		dumper.println(event);
	}

	@Override
	public void flush() {
		dumper.flush();
	}

	@Override
	public void close() {
		dumper.close();
	}

}