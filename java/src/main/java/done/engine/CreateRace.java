package done.engine;

public class CreateRace extends JoinRace {

	public CreateRace() {
		
	}
	
	public CreateRace(String name, String key, Integer carCount,
			String trackName, String password) {
		super(name, key, carCount, trackName, password);
	}
	
	
	@Override
	protected String msgType() {
		return "createRace";
	}

}
