package done.engine;

import done.model.SendMsg;

public class JoinRace extends SendMsg {

	public class BotId {
	
		String name;
		String key;
		
		public BotId() {
			
		}
		
		public BotId(String name, String key) {
			super();
			this.name = name;
			this.key = key;
		}
		
	}

	BotId botId;
	Integer carCount;
	String  trackName;
	String  password;

	public JoinRace() {
		
	}
	
	public JoinRace(String name, String key, Integer carCount) {
		botId = new BotId(name, key); 
		this.carCount = carCount;
	}
	
	public JoinRace(String name, String key, Integer carCount,
			String trackName, String password) {
		this(name, key, carCount);
		this.carCount = carCount;
		this.trackName = trackName;
		this.password = password;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}

}
