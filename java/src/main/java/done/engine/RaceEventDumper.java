package done.engine;

public interface RaceEventDumper {

	
	void println(String event);
	
	void flush();
	
	void close();
	
	
}
