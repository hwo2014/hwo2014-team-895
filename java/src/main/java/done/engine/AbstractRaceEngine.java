package done.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.gson.Gson;

import done.gson.GsonFactory;
import done.model.CarId;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.Join;
import done.model.LapFinished;
import done.model.Ping;
import done.model.SendMsg;
import done.model.SwitchLane;
import done.model.Throttle;
import done.model.Turbo;
import done.model.TurboAvailable;

abstract class AbstractRaceEngine implements RaceEngine {

	private final static Logger LOGGER = LoggerFactory.getLogger(AbstractRaceEngine.class);
	protected static final Ping PING = new Ping();
	protected RaceEventListener raceEventListener;
	protected RaceEventPublisher publisher;
	protected Reader reader;
	private RaceEventDumper eventDumper; 
	protected final Gson gson = GsonFactory.gson();
	private LinkedList<Integer> ticks = Lists.newLinkedList();
	private List<Double> throttleHistory = Lists.newArrayList();
	private List<SendMsg> sendMsgHistory = Lists.newArrayList();
	
	public AbstractRaceEngine() {
		super();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		
		try(BufferedReader br = new BufferedReader(reader)) {
			for(String line; (line = br.readLine()) != null; ) {
				GenericMessageWrapper<?> msg = gson.fromJson(line, GsonFactory.genericMessageWrapperType);
				if (msg.hasTick()) {
					LOGGER.debug("Got tick {} for msgType {}", msg.gameTick(), msg.type() );
					ticks.add(msg.gameTick());
				}
				dumpEvent(line);
				switch (msg.type()) {
				case GAME_INIT:
					raceEventListener.onGameInit((GenericMessageWrapper<GameInit>) msg);
					break;
				case GAME_START:
					raceEventListener.onGameStart((GenericMessageWrapper<Void>) msg);
					break;
				case CRASH:
					raceEventListener.onCrash((GenericMessageWrapper<CarId>) msg);
					break;
				case GAME_END:
					raceEventListener.onGameEnd((GenericMessageWrapper<Object>) msg);
					break;
				case SPAWN:
					raceEventListener.onSpawn((GenericMessageWrapper<CarId>) msg);
					break;
				case LAP_FINISHED:
					raceEventListener.onLapFinished((GenericMessageWrapper<LapFinished>) msg);
					break;
				case CAR_POSITIONS:
					raceEventListener.onCarPositions((GenericMessageWrapper<List<CarPosition>>) msg);
					break;
				case ERROR:
					raceEventListener.onError((GenericMessageWrapper<String>) msg);
					break;
				case JOIN:
					raceEventListener.onJoin();
					break;
				case JOINRACE:
					raceEventListener.onJoinRace();
					break;				
				case YOUR_CAR:
					raceEventListener.onYourCar((GenericMessageWrapper<CarId>) msg);
					break;			
				case FINISH:
					raceEventListener.onFinish((GenericMessageWrapper<CarId>) msg);
					break;	
				case TOURNAMENTEND:
					raceEventListener.onTournamentEnd( throttleHistory, sendMsgHistory );
					break;	
				case TURBO_AVAILABLE:
					raceEventListener.onTurboAvailable((GenericMessageWrapper<TurboAvailable>) msg);
					break;	
				case TURBOSTART:
					raceEventListener.onTurboStart((GenericMessageWrapper<CarId>) msg);
					break;	
				case TURBOEND:
					raceEventListener.onTurboEnd((GenericMessageWrapper<CarId>) msg);
					break;	
				case CREATERACE:
					raceEventListener.onCreateRace();
					break;	
				case DNF:
					// TODO
					raceEventListener.onDnf(null);
					break;	
				default:
					raceEventListener.onUnknown((GenericMessageWrapper<Object>) msg);
					break;
				}			    
			}
		} catch (IOException e) {
			throw new RaceEngineException(e);
		} finally {
			if (eventDumper != null) {
				eventDumper.close();
			}
		}
	}

	private void dumpEvent(String line) {
		if (eventDumper != null) {
			eventDumper.println(line);
			eventDumper.flush();
		}
	}
	
	@Override
	public void addRaceEventListener(RaceEventListener raceEventListener) {
		this.raceEventListener = raceEventListener;
	}
	
	@Override
	public void addRaceEventDumper(RaceEventDumper raceEventDumper) {
		this.eventDumper = raceEventDumper;
	}
	
	@Override
	public void join(String botName, String key) {
		sendMsg(new Join(botName, key));
	}

	@Override
	public void throttle(Double intensity) {
		throttleHistory.add(intensity);
		internalSend(new Throttle(intensity));
	}

	@Override
	public void ping() {
		sendMsg(PING);
	}

	@Override
	public void joinRace(String name, String key, Integer carCount) {
		sendMsg(new JoinRace(name, key, carCount));
	}

	@Override
	public void createRace(String name, String key, Integer carCount,
			String trackName, String password) {
		sendMsg(new CreateRace(name, key, carCount, trackName, password));
	}

	@Override
	public void joinRace(String name, String key, Integer carCount,
			String trackName, String password) {
		sendMsg(new JoinRace(name, key, carCount, trackName, password));
	}

	@Override
	public void turbo(String message) {
		internalSend(new Turbo(message));
		raceEventListener.onTurboActivationRequested();
	}
	
	private void internalSend(SendMsg msg) {
		Integer first = ticks.pollFirst();
		if (first != null) {
			msg.tick(first);
			LOGGER.info("Using tick {} for msg {}", first, msg.toJson() );
			sendMsg(msg);
		} else {
			LOGGER.error("No tick available, sending anyway.. - {}", msg.toJson());
			sendMsg(msg);
		}
	}
	
	void sendMsg(final SendMsg msg) {
		sendMsgHistory.add(msg);
		send(msg);
	}
	
	abstract void send(final SendMsg msg);

	@Override
	public void switchRight() {
		internalSend(new SwitchLane("Right"));
		raceEventListener.onSwitchRightRequested();
	}

	@Override
	public void switchLeft() {
		internalSend(new SwitchLane("Left"));
		raceEventListener.onSwitchLeftRequested();
	}

	@Override
	public Double lastThrottle() {
		return throttleHistory.isEmpty() ? 0d : throttleHistory.get(throttleHistory.size() - 1);
	}
	
	@Override
	public List<Double> throttleHistory() {
		return throttleHistory;
	}	
}