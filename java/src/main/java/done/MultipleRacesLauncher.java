package done;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class MultipleRacesLauncher {

	private final static Logger LOGGER = LoggerFactory.getLogger(MultipleRacesLauncher.class);
	
	private ExecutorService taskExecutor;
	private List<RaceLauncher> launchers = Lists.newArrayList();

	public MultipleRacesLauncher() {
		super();
	}
	
	public void addRaceLauncher(RaceLauncher launcher) {
		launchers.add(launcher);
	}
	
	public MultipleRacesLauncher(RaceLauncher... launchers) {
		super();
		this.launchers = Lists.newArrayList(launchers);
	}

	void launchAll() throws InterruptedException {
		final int numberOfLaunchers = launchers.size();
		taskExecutor = Executors.newFixedThreadPool(numberOfLaunchers);
		for (int i = 0; i < numberOfLaunchers; i++) {
			final int index = i;
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					try {
						launchers.get(index).launch();
					} catch (InterruptedException e) {
						LOGGER.error(e.getLocalizedMessage());
					}
				}
			});
		}
		
		taskExecutor.shutdown();
		taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
	}
	
}