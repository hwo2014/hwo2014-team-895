package done.bot;

import java.io.IOException;
import java.net.UnknownHostException;

import done.engine.RaceEngine;
import done.engine.SocketRaceEngine;

public class BotFactory {

	public static Bot random(String host, int port, String botName,
			String botKey) throws UnknownHostException, IOException {
		return new RandomBot(raceEngine(host, port), botName, botKey);
	}
	
	public static Bot noob(String host, int port, String botName,
			String botKey) throws UnknownHostException, IOException {
		return new NoobBot(raceEngine(host, port), botName, botKey);
	}

	public static Bot noob(String host, int port, String botName,
			String botKey, Double throttle) throws UnknownHostException, IOException {
		return new NoobBot(raceEngine(host, port), botName, botKey, throttle);
	}
	
	public static Bot noob(String host, int port, String botName,
			String botKey, double throttle, boolean useTurbo, boolean switchLanes) throws UnknownHostException, IOException {
		return new NoobBot(raceEngine(host, port), botName, botKey, throttle, useTurbo, switchLanes);
	}
	
	public static Bot brainy(String host, int port, String botName,
			String botKey, BotBrain brain) throws UnknownHostException, IOException {
		return new BrainyBot(raceEngine(host, port), botName, botKey, brain);
	}
	
	public static Bot brainyTurbo(String host, int port, String botName,
			String botKey, BotBrain brain) throws UnknownHostException, IOException {
		return new BrainyTurboBot(raceEngine(host, port), botName, botKey, brain);
	}
	
	public static Bot multiBrain(String host, int port, String botName,
			String botKey, BotBrain brain) throws UnknownHostException, IOException {
		return new MultiBrainBot(raceEngine(host, port), botName, botKey, brain);
	}

	public static RaceEngine raceEngine(String host, int port)
			throws UnknownHostException, IOException {
		RaceEngine raceEngine = new SocketRaceEngine(host, port);
		return raceEngine;
	}
	
}