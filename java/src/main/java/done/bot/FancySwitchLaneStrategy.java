package done.bot;

import done.model.CarPosition;
import done.model.Track;

public class FancySwitchLaneStrategy implements SwitchLaneStrategy {

	@Override
	public LaneCrossingAction execute(GameState gameState, CarPosition carPosition,
			boolean isLeftLaneOrFrontLeftBlocked, boolean isFrontBlocked,
			boolean isRightLaneOrFrontRightBlocked) {
		
		Track track = gameState.track();
		
		if (isFrontBlocked) {
			if (track.nextCurveIsToTheRight(carPosition)
					&& !isRightLaneOrFrontRightBlocked) {
				return LaneCrossingAction.MOVE_RIGHT;
			} else if (track.nextCurveIsToTheLeft(carPosition)
					&& !isLeftLaneOrFrontLeftBlocked) {
				return LaneCrossingAction.MOVE_LEFT;
			} else if (!isLeftLaneOrFrontLeftBlocked) {
				return LaneCrossingAction.MOVE_LEFT;
			} else if (!isRightLaneOrFrontRightBlocked) {
				return LaneCrossingAction.MOVE_RIGHT;
			}
		} else if (track.nextCurveIsToTheRight(carPosition)
				&& !isRightLaneOrFrontRightBlocked) {
			return LaneCrossingAction.MOVE_RIGHT;
		} else if (track.nextCurveIsToTheLeft(carPosition)
				&& !isLeftLaneOrFrontLeftBlocked) {
			return LaneCrossingAction.MOVE_LEFT;
		}
		return LaneCrossingAction.DONT_MOVE;
	}

}