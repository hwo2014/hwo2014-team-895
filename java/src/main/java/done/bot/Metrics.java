package done.bot;

import done.gson.GsonFactory;
import done.model.CarPosition;
import done.model.TrackPiece;
import done.model.TurboAvailable;

public class Metrics {
	
	Integer gameTick;
	Long timestamp;
	CarPosition lastPosition;
	CarPosition position;
	Double distanceToNextBend;
	Float angle;
	TrackPiece piece;
	Boolean nextPieceIsSwitchable = false;
	Double velocity;
	TurboAvailable turboAvailable;
	Boolean turboOn = false;
	Boolean outOTrack = false;
	Boolean isFrontBlocked = false;
	Boolean isLeftLaneOrFrontLeftBlocked = false;
	Boolean isRightLaneOrFrontRightBlocked = false;
	
	Double throttle;
	String botName;
	public Double nextBendCurvature;
	
	@Override
	public String toString() {
		return GsonFactory.gson().toJson(this);
	}

	public boolean isTurboAvailable() {
		return turboAvailable != null;
	}
}