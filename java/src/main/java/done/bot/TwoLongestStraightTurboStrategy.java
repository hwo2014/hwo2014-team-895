package done.bot;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.Track.TrackSegment;
import done.model.TurboAvailable;

public class TwoLongestStraightTurboStrategy implements TurboStrategy {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(TwoLongestStraightTurboStrategy.class);

	@Override
	public void apply(GameState gameState, Metrics metrics, Action action) {
		TurboAvailable turboAvailable = gameState.turboAvailable();
		if (turboAvailable != null) {
			LOGGER.debug("There is a turbo available: {}", turboAvailable);
			if (!metrics.turboOn) {
				LOGGER.debug("Turbo is currently OFF");
				int currentIndex = metrics.piece.index();
				
				List<TrackSegment> straights = gameState.track().straights();
				int index1 = straights.get(0).first().index();
				int index2 = straights.get(1).first().index();
				
				if (currentPositionSurrounds(currentIndex, index1) ||  currentPositionSurrounds(currentIndex, index2)) {
					action.turbo = true;
				}
			} else {
				LOGGER.debug("Turbo is currently ON");
			}
		} else {
			LOGGER.debug("No turbo available.");
		}
	}

	private boolean currentPositionSurrounds(int currentIndex,
			int longestStraightInitIndex) {
		return currentIndex == (longestStraightInitIndex - 1)
				|| currentIndex == longestStraightInitIndex
				|| currentIndex == (longestStraightInitIndex + 1);
	}

}
