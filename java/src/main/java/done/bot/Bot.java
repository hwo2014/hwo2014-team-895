package done.bot;

import done.engine.RaceEngine;

public interface Bot {

	void quickRace();

	void joinRace(Integer carCount);

	void joinRace(String trackName, String password, Integer carCount);

	String key();

	String name();

	void createRace(String trackName, String password, Integer carCount);

	RaceEngine raceEngine();
	
}
