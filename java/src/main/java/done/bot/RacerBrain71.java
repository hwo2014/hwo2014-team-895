package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.Throttle;

public class RacerBrain71 extends FuzzyBotBrain71 {

	private final static Logger LOGGER = LoggerFactory.getLogger(RacerBrain71.class);

	public RacerBrain71(TurboStrategy turboStrategy, SwitchLaneStrategy switchLaneStrategy) {
		super(turboStrategy, switchLaneStrategy);
	}
	
	@Override
	protected void applyThrottlingStrategy(Metrics metrics, GameState gameState,
			Action action) {
		
		
		if (metrics.gameTick == null || spawnedLessThanTicksAgo(20, metrics, gameState)) {

			action.throttle = new Throttle(FULL_THROTTLE);

		} else {
			
			Double longestStraight = gameState.track().longestStraight().length();
			
			
			if (metrics.turboOn && 
					(metrics.velocity > avgVelocity()) &&  
					metrics.distanceToNextBend < (longestStraight * 0.35)) {
				action.throttle = new Throttle(0.05);
				
			} else {
				
				if (metrics.velocity > velocity.getMaximum()) {
					adjustTopSpeed(metrics.velocity * 1.05);
				}
				
				velocity.setInputValue(velocity(metrics));
				nextBendCurvature.setInputValue(metrics.nextBendCurvature);
				distanceToBend.setInputValue(distanceToBend(metrics));
				angle.setInputValue(angle(metrics));
				
				if (gameState.isCarOnStraightPiece()) {
					engineForStraightPiece.process();
				} else {
					engineForBendPiece.process();
				}
				
				Double defuzzifiedThrottle = throttle.defuzzify();
				
				if (defuzzifiedThrottle == Double.NaN) {
					LOGGER.warn("Using default throttle.");
					defuzzifiedThrottle = throttle.getDefaultValue();
				} else {
					if ( defuzzifiedThrottle > 0.95 && defuzzifiedThrottle < FULL_THROTTLE) {
						defuzzifiedThrottle = FULL_THROTTLE;
					}
				}
				
				action.throttle = new Throttle(defuzzifiedThrottle);
			}
			
		}
		LOGGER.debug("velocity:{}, distanceToBend:{}, angle:{} => throttle:{}", metrics.velocity, metrics.distanceToNextBend, metrics.angle, action.throttle.value());
	}

	private Double avgVelocity() {
		return velocityHistory.stream().mapToDouble(v -> v).average().getAsDouble();
	}

	
}
