package done.bot;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.engine.RaceEngine;
import done.model.CarPosition;
import done.model.GenericMessageWrapper;

public class RandomBot extends AbstractBot {

	private final static Logger LOGGER = LoggerFactory.getLogger(RandomBot.class);
	private Random rnd = new Random();
	

	public RandomBot(RaceEngine raceEngine, String name, String key) {
		super(raceEngine, name, key);
	}

	@Override
	void handleCarPositions(
			GenericMessageWrapper<List<CarPosition>> carPositionstMsg,
			Metrics metrics) {
		
		if (gameState().canSwitch() && rnd.nextBoolean()) {
			if (rnd.nextBoolean()) {
				raceEngine().switchLeft();	
			} else {
				raceEngine().switchRight();
			}
		} else if (gameState().turboAvailable() != null && rnd.nextBoolean() ){
			raceEngine().turbo("Suck that!!!");	
		} else {
			Double throttle = rnd.nextDouble();
			raceEngine().throttle(throttle);
		}
		
		LOGGER.info("{}/{} [{}/{}] unb={}, throttle={}, velocity={} ", metrics.position.inPieceDistance()
				, metrics.piece.length()
				, metrics.position.pieceIndex()
				, gameState().race().track().pieces.size()
				, metrics.distanceToNextBend
				, raceEngine().lastThrottle()
				, metrics.velocity
				);
	}
}