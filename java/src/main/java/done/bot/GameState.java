package done.bot;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.SortedMap;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import done.model.Car;
import done.model.CarId;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.LapFinished;
import done.model.Race;
import done.model.Track;
import done.model.TrackPiece;
import done.model.TurboAvailable;

public class GameState {

	private final static Logger LOGGER = LoggerFactory.getLogger(GameState.class);

	private String gameId;
	private Integer gameTick;
	private CarId carId;
	private Car car;
	private Race race;
	private GameInit gameInit;
	private Multimap<Integer, CarPosition> carPositionsPerGameTick = ArrayListMultimap.create();
	private Multimap<Integer, CarId> crashesPerTick = ArrayListMultimap.create();
	private Multimap<Integer, CarId> spawnPerTick = ArrayListMultimap.create();
	private SortedMap<Integer, GenericMessageWrapper<TurboAvailable>> turbosPerTick = Maps.newTreeMap();
	private TrackPiece currentPiece;
	private CarPosition lastPosition;
	private Boolean turboOn = false;
	private Boolean turboRequested = false;
	private Boolean switchLeftRequested = false;
	private Boolean switchRightRequested = false;
	private Boolean isOutOfTrack = false;
	private TurboAvailable turbo = null;
	private Integer lastSpawnTick = 0;
	
	public GameState(String gameId, CarId car) {
		super();
		this.gameId = gameId;
		this.carId = car;
	}
	
	public void onGameInit(GenericMessageWrapper<GameInit> msg) {
		tick(msg);
		gameInit = msg.data();
		this.race = gameInit.race();
		this.race.setup();
		this.car = this.race.car(carId);
		this.race.describe();
	}
	
	private void tick(GenericMessageWrapper<?> msg) {
		if (msg.gameTick() != null) {
			this.gameTick = msg.gameTick(); 
		} else {
			LOGGER.debug("Msg does not tick gameTick is null.");
		}
	}

	public void onGameStart(GenericMessageWrapper<Void> msg) {
		//{"msgType":"gameStart","data":null,"gameId":"9b151436-79a8-4c98-9c16-bff2070df0f8","gameTick":0}
		tick(msg);
		LOGGER.info("Game {} started at {}.", msg.gameId(), new Date());
	}
	
	public Metrics onCarPositions(
			GenericMessageWrapper<List<CarPosition>> carPositionstMsg) {

		List<CarPosition> carPositions = carPositionstMsg.data();
		CarPosition carPosition = carPosition(carId, carPositions);
		currentPiece = track().piece(carPosition.pieceIndex());
		race().onCarPositons(carPositions);
		
		if (car.switchedLane()) {
			switchLeftRequested = false;
			switchRightRequested = false;
		}
		
		Metrics metrics = new Metrics();
		
		// ponto de largada carPositions com tick null
		if (carPositionstMsg.gameTick() == null) {
			carPositionsPerGameTick.putAll(0, carPositions);
			gameTick = 0;
			metrics.velocity = 0d;
		} else {
			carPositionsPerGameTick.putAll(carPositionstMsg.gameTick(), carPositions);
			gameTick = carPositionstMsg.gameTick();
			metrics.velocity = car.velocity();
		}

		//TODO improve and FINISH metrics creation and calculation
		metrics.distanceToNextBend = car.distanceToNextBend(carPosition);
		metrics.piece = currentPiece;
		metrics.lastPosition = lastPosition;
		metrics.position = carPosition;
		metrics.turboAvailable = turboAvailable();
		metrics.turboOn = turboOn;
		metrics.gameTick = gameTick;
		metrics.timestamp = System.currentTimeMillis();
		metrics.angle = carPosition.angle();
		metrics.nextBendCurvature = track().nextCurveAvgCurvature(carPosition);
		
		for (CarPosition otherCarPosition : carPositions) {
			if (otherCarPosition.id() != carPosition.id()) {
				Integer nextPieceIndex = track().nextTrackPieceIndex(
						carPosition);
				Float distanceToNextCar = this.inPieceDistanceToNextCar(
						carPosition, otherCarPosition);
				if (((otherCarPosition.pieceIndex() == carPosition.pieceIndex()) && distanceToNextCar >= -0.5)
						|| otherCarPosition.pieceIndex() == nextPieceIndex) {
					if (otherCarPosition.piecePosition().lane().index() == carPosition
							.piecePosition().lane().index()) {
						metrics.isFrontBlocked = true;
					}
					if (race.track.isRightLane(carPosition)
							|| otherCarPosition.piecePosition().lane().index() == (carPosition
									.piecePosition().lane().index() + 1)) {
						metrics.isRightLaneOrFrontRightBlocked = true;
					}
					if (race.track.isLeftLane(carPosition)
							|| otherCarPosition.piecePosition().lane().index() == (carPosition
									.piecePosition().lane().index() - 1)) {
						metrics.isLeftLaneOrFrontLeftBlocked = true;
					}
				}
			}
		}
		
		lastPosition = carPosition;
		return metrics;
	}
	
	public boolean isCarOnBendPiece() {
		return currentPiece.isBend();
	}
	
	public boolean isCarOnStraightPiece() {
		return currentPiece.isStraight();
	}
	
	public Collection<CarPosition> previousCarPositions() {
		if (gameTick == null || gameTick == 0) {
			return carPositionsPerGameTick.get(0);
		}
		return carPositionsPerGameTick.get(gameTick - 1);
	}

	private CarPosition carPosition(CarId car, Collection<CarPosition> carPositions) {
		Predicate<CarPosition> myPosition = p -> p.id().equals(car);
		Optional<CarPosition> currentPosition = carPositions.stream().filter(myPosition).findFirst();
		return currentPosition.get();
	}

	public boolean canSwitch() {
		if (currentPiece != null && currentPiece.containsSwitch()) {
			return true;
		}
		return false;
	}
	
	public void onCrash(GenericMessageWrapper<CarId> carId) {
		crashesPerTick.put(carId.gameTick(), carId.data());
		if (carId.equals(carId.data())) {
			isOutOfTrack = true;
		}		
		LOGGER.warn("Crashed: {} ", carId);
	}
	
	public void onSpawn(GenericMessageWrapper<CarId> carId) {
		LOGGER.debug("Spawned: {}", carId);
		spawnPerTick.put(carId.gameTick(), carId.data());
		if (carId.equals(carId.data())) {
			lastSpawnTick = carId.gameTick();
			isOutOfTrack = false;
		}
	}
	
	public void onLapFinished(GenericMessageWrapper<LapFinished> lapFinishedMsg) {
		LOGGER.info("Lap Finished. {}", lapFinishedMsg);
	}
	
	public void onFinish(GenericMessageWrapper<CarId> carId) {
		//tick(carId);
	}
	
	public void onDnf(GenericMessageWrapper<Object> carId) {
		//gameTick
	}

	public void onTurboAvailable(GenericMessageWrapper<TurboAvailable> msg) {
		this.turbo = msg.data();
		turbosPerTick.put(msg.gameTick(), msg);
		LOGGER.info("Turbo available = {}", msg.data());
	}

	public void onTurboStart(GenericMessageWrapper<CarId> msg) {
		if (carId.equals(msg.data())) {
			turbo = null;
			turboOn = true;
			turboRequested = false;
		}
		LOGGER.info("Turbo started: {}", msg.data());
	}

	public void onTurboEnd(GenericMessageWrapper<CarId> msg) {
		if (carId.equals(msg.data())) {
			turboOn = false;
		}
		LOGGER.info("Turbo ended: {}", msg.data());
	}

	public void onGameEnd(GenericMessageWrapper<Object> msg) {
		LOGGER.info("Game {} ended at {}.", msg.gameId(), new Date());
		for (Car car : race().cars ) {
			LOGGER.info(String.format(
					"Speedometer [car,min,max,avg] : [%s,%f,%f,%f]", car.id().color(),
					car.getSpeedometer().min(), car.getSpeedometer()
							.max(), car.getSpeedometer().avg()));
		}
	}
	
	public void onTournamentEnd() {
	}

	public String gameId() {
		return this.gameId;
	}

	public Race race() {
		return race;
	}

	public Track track() {
		return race().track();
	}

	public TurboAvailable turboAvailable() {
		return this.turbo;
	}
	
	public Double lastSentThrottle() {
		
		
		return 0d;
	}
	

	public void onTurboRequested() {
		this.turboRequested = true;
	}

	public void onSwitchRightRequested() {
		this.switchRightRequested = true;
		this.switchLeftRequested = false;
	}

	public void onSwitchLeftRequested() {
		this.switchLeftRequested = true;
		this.switchRightRequested = false;
	}

	public Boolean turboRequested() {
		return turboRequested == null ? false : turboRequested;
	}

	public Boolean switchLeftRequested() {
		return switchLeftRequested == null ? false : switchLeftRequested;
	}

	public Boolean switchRightRequested() {
		return switchRightRequested == null ? false : switchRightRequested;
	}

	private Float inPieceDistanceToNextCar(CarPosition yours, CarPosition theirs) {
		Car yourCar = race.carsMap.get(yours.id());
		Car theirCar = race.carsMap.get(theirs.id());
		Float yourFrontBumperPos = yours.inPieceDistance() + yourCar.dimensions().guideFlagPosition;
		Float theirRearBumperPos = theirs.inPieceDistance()-(theirCar.dimensions().length - theirCar.dimensions().guideFlagPosition);
		return theirRearBumperPos - yourFrontBumperPos;
	}

	public Integer lastSpawnTick() {
		return lastSpawnTick;
	}

	public Multimap<Integer, CarId> getCrashesPerTick() {
		return crashesPerTick;
	}

	public Multimap<Integer, CarId> getSpawnPerTick() {
		return spawnPerTick;
	}

	public SortedMap<Integer, GenericMessageWrapper<TurboAvailable>> getTurbosPerTick() {
		return turbosPerTick;
	}
	
	public Boolean isTurboOn() {
		return turboOn;
	}

	public CarPosition lastPosition() {
		return lastPosition;
	}
}
