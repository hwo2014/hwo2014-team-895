package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.TurboAvailable;

public class DistanceToBendTurboStrategy implements TurboStrategy {

	private final static Logger LOGGER = LoggerFactory.getLogger(DistanceToBendTurboStrategy.class);
	private static final int DISTANCE_TO_NEXT_BEND_THRESHOLD = 100;
	private double distanceToNextBend = DISTANCE_TO_NEXT_BEND_THRESHOLD;
	
	public DistanceToBendTurboStrategy() {
		super();
	}

	public DistanceToBendTurboStrategy(double distanceToNextBend) {
		super();
		this.distanceToNextBend = distanceToNextBend;
	}
	
	@Override
	public void apply(GameState gameState, Metrics metrics, Action action) {
		if (gameState.isCarOnStraightPiece()) {
			TurboAvailable turboAvailable = gameState.turboAvailable();
			if (turboAvailable != null ) {
				LOGGER.debug("There is a turbo available: {}", turboAvailable);
				LOGGER.debug("Turbo is currently OFF");
				if (metrics.distanceToNextBend  >= distanceToNextBend) {
					action.turbo = true;
				}
			}
		}
	}
}