package done.bot;

import java.util.List;

import com.google.common.collect.Lists;

import done.engine.RaceEngine;
import done.model.Action;

public abstract class BaseBrain implements BotBrain {

	protected static final double FULL_THROTTLE = 1d;
	
	private TurboStrategy turboStrategy;
	private SwitchLaneStrategy switchLaneStrategy;
	protected List<Double> throttleHistory = Lists.newArrayList();
	protected List<Double> velocityHistory = Lists.newArrayList();

	public BaseBrain(TurboStrategy turboStrategy,
			SwitchLaneStrategy switchLaneStrategy) {
		super();
		this.turboStrategy = turboStrategy;
		this.switchLaneStrategy = switchLaneStrategy;
	}

	@Override
	public void setTurboStrategy(TurboStrategy turboStrategy) {
		this.turboStrategy = turboStrategy;
	}
	
	@Override
	public void startBrain(GameState gameState) {
		
	}

	
	@Override
	public Action analyse(Metrics metrics, GameState gameState,
			RaceEngine raceEngine) {
		velocityHistory.add(metrics.velocity);
		this.throttleHistory =  raceEngine.throttleHistory();
		Action action = new Action();
		beforeApplyThrottle(metrics);
		applyThrottlingStrategy(metrics, gameState, action);
		applyTurboStrategy(gameState, metrics, action);
		applyLaneCrossingStrategy(metrics, gameState, action);
		return action;
	}

	protected void beforeApplyThrottle(Metrics metrics) {
	}

	protected void applyLaneCrossingStrategy(Metrics metrics,
			GameState gameState, Action action) {
		switch (switchLaneStrategy.execute(gameState, metrics.position,
				metrics.isLeftLaneOrFrontLeftBlocked, metrics.isFrontBlocked,
				metrics.isRightLaneOrFrontRightBlocked)) {
			case MOVE_RIGHT:
				action.switchRight = true;
				break;
			case MOVE_LEFT:
				action.switchLeft = true;
				break;
			case DONT_MOVE:
			default:
				action.switchLeft = false;
				action.switchRight = false;
				break;
			}
	}

	protected void applyTurboStrategy(GameState gameState, Metrics metrics, Action action) {
		this.turboStrategy.apply(gameState, metrics, action);
	}

	abstract void applyThrottlingStrategy(Metrics metrics, GameState gameState,
			Action action);

	protected Double latestPositiveVelocity() {
		for (int i = velocityHistory.size() - 1; i >= 0; i--) {
			Double v = velocityHistory.get(i);
			if ( v > 0 ) {
				return v;
			}
		}
		return 0d;
	}

	public Double dragConstant() {
		return dragConstant( velocityHistory.get(1), velocityHistory.get(2), FULL_THROTTLE );
	}

	public Double mass() {
		return mass(velocityHistory.get(2), velocityHistory.get(3), FULL_THROTTLE, dragConstant());
	}

	public Double dragConstant(Double v1, Double v2, Double throttle) {
		return (v1 - (v2 - v1)) / (v1 * v1) * throttle;
	}

	public Double mass(Double v2, Double v3, Double throttle, Double dragConstant) {
		return 1.0d / (Math.log(( v3 - (throttle / dragConstant)) / (v2 - (throttle / dragConstant))) / (-dragConstant));
	}

	public Double terminalVelocityForThrottle(Double throttle, Double dragConstant) {
		//v = h/k
		return throttle / dragConstant;
	}

	protected boolean spawnedLessThanTicksAgo(int ticks, Metrics metrics,
			GameState gameState) {
				return ( metrics.gameTick - gameState.lastSpawnTick() ) <= ticks;
			}

}