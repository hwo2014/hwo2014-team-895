package done.bot;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import done.engine.RaceEngine;
import done.engine.RaceEventListener;
import done.engine.RaceEventPublisher;
import done.model.CarId;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.LapFinished;
import done.model.SendMsg;
import done.model.TurboAvailable;

public abstract class AbstractBot implements Bot, RaceEventListener {

	private final static Logger LOGGER = LoggerFactory.getLogger(AbstractBot.class);

	protected Multimap<String, GameState> gameStates = ArrayListMultimap.<String, GameState>create();
	protected String name;
	protected String key;
	protected RaceEngine raceEngine;
	protected RaceEventPublisher publisher;
	
	// TODO GameContext / RaceContext / RaceState
	private GameState gameState;
	protected CarId car;
	
	public AbstractBot(RaceEngine raceEngine, String name, String key) {
		Preconditions.checkNotNull(raceEngine);
		this.raceEngine = raceEngine;
		this.raceEngine.addRaceEventListener(this);
		this.name = name;
		this.key = key;
	}
	
	@Override
	public void quickRace() {
		LOGGER.info("Starting quick race.");
		raceEngine.join(name, key);
		raceEngine.run();
	}

	@Override
	public void joinRace(Integer carCount) {
		LOGGER.info("{} joining race with {} other bot(s)", name, carCount - 1);
		raceEngine.joinRace(name, key, carCount);
		raceEngine.run();
	}

	@Override
	public void joinRace(String trackName, String password, Integer carCount) {
		LOGGER.info("{} joining race with {} other bot(s) on track {}", name, carCount - 1, trackName);
		raceEngine.joinRace(name, key, carCount, trackName, password);
		raceEngine.run();
	}

	@Override
	public void createRace(String trackName, String password, Integer carCount) {
		LOGGER.info("{} creating new race with {} other bot(s) on track {}", name, carCount - 1, trackName);
		raceEngine.createRace(name, key, carCount, trackName, password);
		raceEngine.run();
	}
	
	@Override
	public String name() {
		return name;
	}

	@Override
	public String key() {
		return key;
	}

	@Override
	public void onJoin() {
		LOGGER.debug("Got join ACKL");
	}

	@Override
	public void onYourCar(GenericMessageWrapper<CarId> carId) {
		car = carId.data();
		LOGGER.info("My car is: " + carId.data());
	}

	@Override
	public void onGameInit(GenericMessageWrapper<GameInit> gameInitMsg) {
		gameState = new GameState(gameInitMsg.gameId(), car);
		gameState.onGameInit(gameInitMsg);
		
		
		//raceEngine.ping();
	}

	@Override
	public void onGameStart(GenericMessageWrapper<Void> msg) {
		gameState.onGameStart(msg);
		
		//FIXME Será que o bot derrapa se largar com full throttle ?
		raceEngine.throttle(1d);
	}

	@Override
	public void onCrash(GenericMessageWrapper<CarId> carId) {
		gameState.onCrash(carId);
	}

	@Override
	public void onSpawn(GenericMessageWrapper<CarId> carId) {
		gameState.onSpawn(carId);
	}

	@Override
	public void onLapFinished(GenericMessageWrapper<LapFinished> msg) {
		gameState.onLapFinished(msg);
	}

	@Override
	public void onDnf(GenericMessageWrapper<Object> carId) {
		LOGGER.error("DNF!!!");
	}

	@Override
	public void onFinish(GenericMessageWrapper<CarId> carId) {
		LOGGER.info("Finished.");
		raceEngine.ping();
	}

	@Override
	public void onTournamentEnd(List<Double> throttleHistory, List<SendMsg> sendMsgHistory) {
		LOGGER.info("Tournament ended.");
		raceEngine.ping();
		double min = throttleHistory
			.stream()
			.mapToDouble(u -> u.doubleValue())
			.min().getAsDouble();
		double max = throttleHistory
			.stream()
			.mapToDouble(u -> u.doubleValue())
			.max().getAsDouble();
		double avg = throttleHistory
			.stream()
			.mapToDouble(u -> u.doubleValue())
			.average().getAsDouble();
		LOGGER.info("Throttle stats: min={},max={},avg={}", min, max, avg);
	}

	@Override
	public void onGameEnd(GenericMessageWrapper<Object> msg) {
		gameState.onGameEnd(msg);
		gameStates.put(gameState.gameId(), gameState);
	}

	@Override
	public void onError(GenericMessageWrapper<String> error) {
		LOGGER.warn(error.data());
		//FIXME o q fazer quando der erro?? Para a engine ??
	}

	@Override
	public void onUnknown(GenericMessageWrapper<Object> unknown) {
		LOGGER.warn("Unknown...");
		raceEngine.ping();
	}

	@Override
	public void onJoinRace() {
		LOGGER.debug("Got joinRace ACK. Pinging back.");
	}

	@Override
	public RaceEngine raceEngine() {
		return raceEngine;
	}

	@Override
	public void onTurboAvailable(GenericMessageWrapper<TurboAvailable> msg) {
		gameState.onTurboAvailable(msg);
	}
	
	@Override
	public void onTurboStart(GenericMessageWrapper<CarId> msg) {
		gameState.onTurboStart(msg);
	}

	@Override
	public void onTurboEnd(GenericMessageWrapper<CarId> msg) {
		gameState.onTurboEnd(msg);
	}
	
	protected GameState gameState() {
		return gameState;
	}

	@Override
	public void onCreateRace() {
		LOGGER.debug("Got create race ACK");
		raceEngine.ping();
	}

	@Override
	public void onCarPositions(GenericMessageWrapper<List<CarPosition>> carPositionstMsg) {
		Metrics metrics = gameState().onCarPositions(carPositionstMsg);
		LOGGER.info("{}", metrics);

		//if(metrics.gameTick != null) {
		handleCarPositions(carPositionstMsg, metrics);
		//}
		
 
	}
	
	abstract void handleCarPositions(GenericMessageWrapper<List<CarPosition>> carPositionstMsg, Metrics metrics);

	@Override
	public void onTurboActivationRequested() {
		gameState().onTurboRequested();
	}

	@Override
	public void onSwitchLeftRequested() {
		gameState().onSwitchLeftRequested();
	}

	@Override
	public void onSwitchRightRequested() {
		gameState().onSwitchRightRequested();
	}

}


