package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fuzzylite.Engine;
import com.fuzzylite.term.Term;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

import done.model.Action;
import done.model.Throttle;

public abstract class BaseFuzzyBrain extends BaseBrain {

	final static Logger LOGGER = LoggerFactory.getLogger(BaseFuzzyBrain.class);
	protected Engine engineForStraightPiece;
	protected Engine engineForBendPiece;
	protected InputVariable velocity;
	protected OutputVariable throttle;
	protected InputVariable distanceToBend;
	protected InputVariable angle;
	protected InputVariable turbo;
	protected InputVariable nextBendCurvature;

	@Override
	public void startBrain(GameState gameState) {
		Double maxDistance = gameState.track().longestStraight().length();
		adjustMaxDistanceToBend(maxDistance);
	}
	
	public BaseFuzzyBrain(TurboStrategy turboStrategy, SwitchLaneStrategy switchLaneStrategy) {
		super(turboStrategy, switchLaneStrategy);
	}

	public Trapezoid velocityHighTerm() {
		return (Trapezoid) velocity.getTerm("HIGH");
	}

	public Trapezoid velocityMediumTerm() {
		return (Trapezoid) velocity.getTerm("MEDIUM");
	}

	public Trapezoid velocityLowTerm() {
		return (Trapezoid) velocity.getTerm("LOW");
	}

	public Term distanceToBendLongTerm() {
		return distanceToBend.getTerm("LONG");
	}

	public Term distanceToBendMediumTerm() {
		return distanceToBend.getTerm("MEDIUM");
	}

	public Term distanceToBendShortTerm() {
		return distanceToBend.getTerm("SHORT");
	}
	
	protected void adjustMaxDistanceToBend(double maxDistance) {
		LOGGER.info("Adjusting maxDistance to : " + maxDistance);
		double oldMaxDistance = distanceToBendMaxValue();
		distanceToBend.setRange(0.000, maxDistance);
		adjustTerm(distanceToBendShortTerm(), maxDistance, oldMaxDistance);
		adjustTerm(distanceToBendMediumTerm(), maxDistance, oldMaxDistance);
		adjustTerm(distanceToBendLongTerm(), maxDistance, oldMaxDistance);
		engineForStraightPiece.restart();
		LOGGER.info("maxDistance adjusted: " + distanceToBend.toString());
	}

	public void adjustTerm(Term t, double maxDistance, double oldMaxDistance) {
		
		if (t instanceof Triangle) {
			Triangle triangle = (Triangle) t;
			if ("SHORT".equals(triangle.getName())) {
				triangle.setA(0);
			} else {
				triangle.setA( (triangle.getA() / oldMaxDistance) * maxDistance);
			}
			triangle.setB( (triangle.getB() / oldMaxDistance) * maxDistance);
			if ("LONG".equals(triangle.getName())) {
				triangle.setC(maxDistance);
			} else {
				triangle.setC((triangle.getC() / oldMaxDistance) * maxDistance);
			}
		}

		if (t instanceof Trapezoid) {
			Trapezoid trapezoid = (Trapezoid) t;
			if ("SHORT".equals(trapezoid.getName())) {
				trapezoid.setA(0);
			} else {
				trapezoid.setA( (trapezoid.getA() / oldMaxDistance) * maxDistance);
			}

			trapezoid.setB( (trapezoid.getB() / oldMaxDistance) * maxDistance);
			trapezoid.setC( (trapezoid.getC() / oldMaxDistance) * maxDistance);
			
			if ("LONG".equals(trapezoid.getName())) {
				trapezoid.setD(maxDistance);
			} else {
				trapezoid.setD( (trapezoid.getD() / oldMaxDistance) * maxDistance);
			}
		}
	}
	
	private double distanceToBendMaxValue() {
		Term t = distanceToBendLongTerm();
		if (t instanceof Trapezoid) {
			return ((Trapezoid)t).getD();
		}
		
		if (t instanceof Triangle) {
			return ((Triangle)t).getC();
		}
		throw new IllegalStateException();
	}

	public void adjustTopSpeed(double topSpeed) {
		
		LOGGER.info("Adjusting topSpeed to : " + topSpeed);
		
		double oldTopSpeed = velocityHighTerm().getD();

		velocity.setRange(0.000, topSpeed);
		velocityLowTerm().setA(0);
		velocityLowTerm().setB(
				(velocityLowTerm().getB() / oldTopSpeed) * topSpeed);
		velocityLowTerm().setC(
				(velocityLowTerm().getC() / oldTopSpeed) * topSpeed);
		velocityLowTerm().setD(
				(velocityLowTerm().getD() / oldTopSpeed) * topSpeed);

		velocityMediumTerm().setA(
				(velocityMediumTerm().getA() / oldTopSpeed) * topSpeed);
		velocityMediumTerm().setB(
				(velocityMediumTerm().getB() / oldTopSpeed) * topSpeed);
		velocityMediumTerm().setC(
				(velocityMediumTerm().getC() / oldTopSpeed) * topSpeed);
		velocityMediumTerm().setD(
				(velocityMediumTerm().getD() / oldTopSpeed) * topSpeed);

		velocityHighTerm().setA(
				(velocityHighTerm().getA() / oldTopSpeed) * topSpeed);
		velocityHighTerm().setB(
				(velocityHighTerm().getB() / oldTopSpeed) * topSpeed);
		velocityHighTerm().setC(
				(velocityHighTerm().getC() / oldTopSpeed) * topSpeed);
		velocityHighTerm().setD(topSpeed);

		engineForBendPiece.restart();
		engineForStraightPiece.restart();
		LOGGER.info("topSpeed adjusted: " + velocity.toString());
	}

	protected double velocity(Metrics metrics) {

		double v = metrics.velocity;
		if (v == velocity.getMinimum()) {
			v = velocity.getMinimum() + 0.1;
		} else if (v < velocity.getMinimum()) {
			v = latestPositiveVelocity();
		} else if (v >= velocity.getMaximum()) {
			v = velocity.getMaximum() - 0.1;
		}

		return v;
	}

	public float angle(Metrics metrics) {
		float angle2 = Math.abs(metrics.angle);

		if (angle2 <= angle.getMinimum()) {
			angle2 = (float) (angle.getMinimum() + 0.001);
		}
		if (angle2 >= angle.getMaximum()) {
			angle2 = (float) (angle.getMaximum() - 0.001);
		}

		return Math.abs(angle2);
	}

	public Double distanceToBend(Metrics metrics) {
		Double distanceToNextBend = metrics.distanceToNextBend;
		if (distanceToNextBend <= distanceToBend.getMinimum()) {
			distanceToNextBend = distanceToBend.getMinimum() + 0.1;
		}
		if (distanceToNextBend >= distanceToBend.getMaximum()) {
			distanceToNextBend = distanceToBend.getMaximum() - 0.1;
		}
		return distanceToNextBend;
	}

//	@Override
//	protected void beforeApplyThrottle(Metrics metrics) {
//		if (metrics.velocity > velocity.getMaximum()) {
//			adjustTopSpeed(metrics.velocity * 1.05);
//		}
//	}
	
	@Override
	protected void applyThrottlingStrategy(Metrics metrics,
			GameState gameState, Action action) {

		if(metrics.turboOn) {
			
			LOGGER.debug("Turbo enabled");
		}
		
		if (metrics.gameTick == null || spawnedLessThanTicksAgo(20, metrics, gameState)) {

			action.throttle = new Throttle(FULL_THROTTLE);

		} else {
		
		
			velocity.setInputValue(velocity(metrics));
			distanceToBend.setInputValue(distanceToBend(metrics));
			angle.setInputValue(angle(metrics));
	
			if (gameState.isCarOnStraightPiece()) {
				engineForStraightPiece.process();
			} else {
				engineForBendPiece.process();
			}
	
			Double defuzzifiedThrottle = throttle.defuzzify();
			if (defuzzifiedThrottle == Double.NaN) {
				defuzzifiedThrottle = throttle.getDefaultValue();
			}
	
			action.throttle = new Throttle(defuzzifiedThrottle);
		
		}
		
		LOGGER.debug("velocity:{}, distanceToBend:{}, angle:{} => throttle:{}", metrics.velocity, metrics.distanceToNextBend, metrics.angle, action.throttle.value());

	}

}