package done.bot;

import done.engine.RaceEngine;
import done.model.Action;

public interface BotBrain {

	void startBrain(GameState gameState);
	
	Action analyse(Metrics metrics, GameState gameState, RaceEngine raceEngine);

	void setTurboStrategy(TurboStrategy turboStrategy);
	

}
