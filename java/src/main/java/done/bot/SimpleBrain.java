package done.bot;

import done.engine.RaceEngine;
import done.model.Action;
import done.model.Throttle;

public class SimpleBrain implements BotBrain {

	@Override
	public Action analyse(Metrics metrics, GameState gameState,
			RaceEngine raceEngine) {
		Action action = new Action();
		action.throttle = new Throttle(0.65d); 
		return action;
	}

	@Override
	public void startBrain(GameState gameState) {
	
	}

	@Override
	public void setTurboStrategy(TurboStrategy turboStrategy) {
		// TODO Auto-generated method stub
		
	}
	
}
