package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fuzzylite.Engine;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.norm.s.AlgebraicSum;
import com.fuzzylite.norm.s.EinsteinSum;
import com.fuzzylite.norm.t.EinsteinProduct;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Constant;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

import done.engine.RaceEngineException;

public class FuzzyBotBrain4 extends BaseFuzzyBrain {

	final static Logger LOGGER = LoggerFactory.getLogger(FuzzyBotBrain4.class);


	public FuzzyBotBrain4(TurboStrategy turboStrategy, SwitchLaneStrategy switchLaneStrategy) {
		super(turboStrategy, switchLaneStrategy);
		
		engineForStraightPiece = new Engine();
		engineForStraightPiece.setName("Straight");
		engineForBendPiece = new Engine();
		engineForBendPiece.setName("bend");
		
		velocity = new InputVariable();
		velocity.setEnabled(true);
		velocity.setName("Velocity");
		velocity.setRange(0.000, 15.000);
		velocity.addTerm(new Trapezoid("LOW", 0.000, 1.850, 3.850, 5.000));
		velocity.addTerm(new Trapezoid("MEDIUM", 2.800, 5.750, 8.250, 12.000));
		velocity.addTerm(new Trapezoid("HIGH", 8.000, 10.750, 12.750, 15.000));
		engineForStraightPiece.addInputVariable(velocity);
		engineForBendPiece.addInputVariable(velocity);

		distanceToBend = new InputVariable();
		distanceToBend.setEnabled(true);
		distanceToBend.setName("DistanceToBend");
		distanceToBend.setRange(0.000, 1000.000);
		distanceToBend.addTerm(new Triangle("SHORT", 0.000, 50.000, 150.000));
		distanceToBend.addTerm(new Triangle("MEDIUM", 100.000, 280.000, 450.000));
		distanceToBend.addTerm(new Triangle("LONG", 400.000, 500.000, 1000.000));
		engineForStraightPiece.addInputVariable(distanceToBend);

		angle = new InputVariable();
		angle.setEnabled(true);
		angle.setName("Angle");
		angle.setRange(0.000, 90.000);
		angle.addTerm(new Trapezoid("LOW", 0.000, 0.000, 5.000, 10.000));
		angle.addTerm(new Trapezoid("MEDIUM", 5.000, 10.000, 20.000, 30.000));
		angle.addTerm(new Trapezoid("HIGH", 25.000, 35.000, 90.000, 90.000));
		engineForBendPiece.addInputVariable(angle);
		
		turbo = new InputVariable();
		turbo.setEnabled(false);
		turbo.setName("Turbo");
		turbo.setRange(0.000, 1.000);
		turbo.addTerm(new Constant("ON", 1.000));
		turbo.addTerm(new Constant("OFF", 0.000));
		//engine.addInputVariable(turbo);		
		
		throttle = new OutputVariable();
		throttle.setEnabled(true);
		throttle.setName("THROTTLE");
		throttle.setRange(0.000, 1.000);
		throttle.fuzzyOutput().setAccumulation(new AlgebraicSum());
		throttle.setDefuzzifier(new Centroid(200));
		throttle.setDefaultValue(0.500);
		throttle.setLockValidOutput(false);
		throttle.setLockOutputRange(false);
		throttle.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));
		throttle.addTerm(new Triangle("MEDIUM", 0.300, 0.450, 0.650));
		throttle.addTerm(new Triangle("LOW", 0.000, 0.200, 0.400));
		engineForStraightPiece.addOutputVariable(throttle);
		engineForBendPiece.addOutputVariable(throttle);
		
		RuleBlock straightRuleBlock = new RuleBlock();
		straightRuleBlock.setEnabled(true);
		straightRuleBlock.setName("straightRuleBlock");
		straightRuleBlock.setConjunction(new EinsteinProduct());
		straightRuleBlock.setDisjunction(new EinsteinSum());
		straightRuleBlock.setActivation(new EinsteinProduct());
		straightRuleBlock.addRule(Rule.parse("if Velocity is HIGH and DistanceToBend is SHORT then THROTTLE is LOW", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is HIGH and DistanceToBend is MEDIUM then THROTTLE is HIGH", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is HIGH and DistanceToBend is LONG then THROTTLE is extremely HIGH", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is MEDIUM and DistanceToBend is SHORT then THROTTLE is LOW", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is MEDIUM and DistanceToBend is MEDIUM then THROTTLE is HIGH", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is MEDIUM and DistanceToBend is LONG then THROTTLE is extremely HIGH", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is LOW and DistanceToBend is SHORT then THROTTLE is extremely MEDIUM", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is LOW and DistanceToBend is MEDIUM then THROTTLE is HIGH", engineForStraightPiece));
		straightRuleBlock.addRule(Rule.parse("if Velocity is LOW and DistanceToBend is LONG then THROTTLE is extremely HIGH", engineForStraightPiece));
		engineForStraightPiece.addRuleBlock(straightRuleBlock);
		
		RuleBlock bendRuleBlock = new RuleBlock();
		bendRuleBlock.setEnabled(true);
		bendRuleBlock.setName("bendRuleBlock");
		bendRuleBlock.setConjunction(new EinsteinProduct());
		bendRuleBlock.setDisjunction(new EinsteinSum());
		bendRuleBlock.setActivation(new EinsteinProduct());
		bendRuleBlock.addRule(Rule.parse("if Velocity is LOW and Angle is LOW then THROTTLE is HIGH", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is LOW and Angle is MEDIUM then THROTTLE is MEDIUM", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is LOW and Angle is HIGH then THROTTLE is extremely LOW", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is MEDIUM and Angle is LOW then THROTTLE is MEDIUM", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is MEDIUM and Angle is MEDIUM then THROTTLE is LOW", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is MEDIUM and Angle is HIGH then THROTTLE is extremely LOW", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is HIGH and Angle is LOW then THROTTLE is MEDIUM", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is HIGH and Angle is MEDIUM then THROTTLE is extremely LOW", engineForBendPiece));
		bendRuleBlock.addRule(Rule.parse("if Velocity is HIGH and Angle is HIGH then THROTTLE is extremely LOW", engineForBendPiece));
		engineForBendPiece.addRuleBlock(bendRuleBlock);
		
		StringBuilder status = new StringBuilder();
		if (!engineForStraightPiece.isReady(status)) {
			LOGGER.error("Engine engineForStraightPiece not ready. The following errors were encountered: {}" , status);
			throw new RaceEngineException(status.toString());
		}
		if (!engineForBendPiece.isReady(status)) {
			LOGGER.error("Engine engineForBendPiece not ready. The following errors were encountered: {}" , status);
			throw new RaceEngineException(status.toString());
		}
	}

	
}