package done.bot;

import done.model.Action;

public class FixedPieceTurboStrategy implements TurboStrategy {

	
	int pieceIndex = 24;
	
	public FixedPieceTurboStrategy(int pieceIndex) {
		super();
		this.pieceIndex = pieceIndex;
	}

	@Override
	public void apply(GameState gameState, Metrics metrics, Action action) {
		if ( metrics.piece.index() == pieceIndex ) {
			action.turbo = true;
		}
	}

}
