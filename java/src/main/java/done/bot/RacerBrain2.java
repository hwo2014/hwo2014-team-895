package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.Throttle;

public class RacerBrain2 extends FuzzyBotBrain5 {

	final static Logger LOGGER = LoggerFactory.getLogger(RacerBrain2.class);

	public RacerBrain2(TurboStrategy turboStrategy,
			SwitchLaneStrategy switchLaneStrategy) {
		super(turboStrategy, switchLaneStrategy);
	}

	@Override
	protected void applyThrottlingStrategy(Metrics metrics,
			GameState gameState, Action action) {

		Double velocity2 = metrics.velocity;

		if (metrics.gameTick == null || spawnedLessThanTicksAgo(20, metrics, gameState)) {

			action.throttle = new Throttle(FULL_THROTTLE);

		} else {


			if (metrics.turboOn) {
				
				LOGGER.debug("");
				
			}

			
			if (metrics.velocity > velocity.getMaximum()) {
				adjustTopSpeed(metrics.velocity * 1.05);
			}
			
			velocity.setInputValue(velocity(metrics));
			distanceToBend.setInputValue(distanceToBend(metrics));
			angle.setInputValue(angle(metrics));
			
			if (gameState.isCarOnStraightPiece()) {
				engineForStraightPiece.process();
			} else {
				engineForBendPiece.process();
			}
			
			Double defuzzifiedThrottle = throttle.defuzzify();

			if (defuzzifiedThrottle == Double.NaN) {
				LOGGER.warn("Using default throttle.");
				defuzzifiedThrottle = throttle.getDefaultValue();
			} else {
				if ( defuzzifiedThrottle >= 0.95 && defuzzifiedThrottle < FULL_THROTTLE) {
					defuzzifiedThrottle = FULL_THROTTLE;
				}
			}
			
			action.throttle = new Throttle(defuzzifiedThrottle);
		}

		LOGGER.debug("velocity:{}, distanceToBend:{}, angle:{} => throttle:{}",
				velocity2, distanceToBend.getInputValue(),
				angle.getInputValue(), action.throttle.value());
	}

}
