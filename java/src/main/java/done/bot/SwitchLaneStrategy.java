package done.bot;

import done.model.CarPosition;

public interface SwitchLaneStrategy {

	public abstract LaneCrossingAction execute(GameState gameState,
			CarPosition carPosition, boolean isLeftLaneOrFrontLeftBlocked,
			boolean isFrontBlocked, boolean isRightLaneOrFrontRightBlocked);

}