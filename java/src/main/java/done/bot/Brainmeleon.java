package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.Throttle;

public class Brainmeleon extends FuzzyBotBrain5 {

	public Brainmeleon(TurboStrategy turboStrategy, SwitchLaneStrategy switchLaneStrategy) {
		super(turboStrategy, switchLaneStrategy);
	}

	private final static Logger LOGGER = LoggerFactory.getLogger(Brainmeleon.class);
	private final SwitchLaneStrategy crossingStrategy = new FancySwitchLaneStrategy();
	
	Boolean topSpeedAdjusted = false;
	
	@Override
	protected void applyThrottlingStrategy(Metrics metrics, GameState gameState,
			Action action) {
		
		velocityHistory.add(metrics.velocity);
		
		if (metrics.gameTick == null || metrics.gameTick < 20) {
			
			action.throttle = new Throttle(FULL_THROTTLE);
			
		} else {
			
			adjustTopSpeed();
			
			velocity.setInputValue(metrics.velocity);
			distanceToBend.setInputValue(metrics.distanceToNextBend);
			angle.setInputValue(Math.abs(metrics.angle));
			
			if (gameState.isCarOnStraightPiece()) {
				engineForStraightPiece.process();
			} else {
				engineForBendPiece.process();
			}
			Double defuzzifiedThrottle = throttle.defuzzify();
			if (defuzzifiedThrottle == Double.NaN) {
				LOGGER.warn("Using default throttle.");
				defuzzifiedThrottle = throttle.getDefaultValue();
			}
			
			if (metrics.turboOn) {
				// adjust throttle ???
				//defuzzifiedThrottle = defuzzifiedThrottle *  
			}
			
			action.throttle = new Throttle(defuzzifiedThrottle);
		}
		LOGGER.debug("velocity:{}, distanceToBend:{}, angle:{} => throttle:{}", metrics.velocity, metrics.distanceToNextBend, metrics.angle, action.throttle.value());
	}

	public void adjustTopSpeed() {
		if (!topSpeedAdjusted) {
			double oldTopSpeed = velocityHighTerm().getD();
			double topSpeed = terminalVelocityForThrottle(FULL_THROTTLE, dragConstant());

			velocity.setRange(0.000, topSpeed);
			velocityLowTerm().setA(0);
			velocityLowTerm().setB((velocityLowTerm().getB()/oldTopSpeed) * topSpeed );
			velocityLowTerm().setC((velocityLowTerm().getC()/oldTopSpeed) * topSpeed);
			velocityLowTerm().setD((velocityLowTerm().getD()/oldTopSpeed) * topSpeed);
			
			velocityMediumTerm().setA((velocityMediumTerm().getA()/oldTopSpeed) * topSpeed);
			velocityMediumTerm().setB((velocityMediumTerm().getB()/oldTopSpeed) * topSpeed);
			velocityMediumTerm().setC((velocityMediumTerm().getC()/oldTopSpeed) * topSpeed);
			velocityMediumTerm().setD((velocityMediumTerm().getD()/oldTopSpeed) * topSpeed);
			
			velocityHighTerm().setA((velocityHighTerm().getA()/oldTopSpeed) * topSpeed);
			velocityHighTerm().setB((velocityHighTerm().getB()/oldTopSpeed) * topSpeed);
			velocityHighTerm().setC((velocityHighTerm().getC()/oldTopSpeed) * topSpeed);
			velocityHighTerm().setD(topSpeed);
			
			engineForBendPiece.restart();
			engineForStraightPiece.restart();
			
			LOGGER.info("##################");
			LOGGER.info(velocity.toString());
			LOGGER.info("##################");
			
			topSpeedAdjusted = true;
		}
	}

	@Override
	protected void applyLaneCrossingStrategy(Metrics metrics,
			GameState gameState, Action action) {
		switch (crossingStrategy.execute(gameState, metrics.position,
				metrics.isLeftLaneOrFrontLeftBlocked, metrics.isFrontBlocked,
				metrics.isRightLaneOrFrontRightBlocked)) {
			case MOVE_RIGHT:
				action.switchRight = true;
				break;
			case MOVE_LEFT:
				action.switchLeft = true;
				break;
			case DONT_MOVE:
			default:
				action.switchLeft = false;
				action.switchRight = false;
				break;
			}
	}
	
	public Double dragConstant() {
		return dragConstant( velocityHistory.get(1), velocityHistory.get(2), FULL_THROTTLE );
	}
	
	public Double mass() {
		return mass(velocityHistory.get(2), velocityHistory.get(3), FULL_THROTTLE, dragConstant());
	}
	
	/**
	 * k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;
	 * @param v1
	 * @param v2
	 * @param throttle
	 * @return
	 */
	public Double dragConstant(Double v1, Double v2, Double throttle) {
		return (v1 - (v2 - v1)) / (v1 * v1) * throttle;
	}

	/**
	 * m = 1.0 / ( ln( ( v(3) - ( h / k ) ) / ( v(2) - ( h / k ) ) ) / ( -k ) )
	 * @param v2
	 * @param v3
	 * @param throttle
	 * @param dragConstant
	 * @return
	 */
	public Double mass(Double v2, Double v3, Double throttle, Double dragConstant) {
		return 1.0d / (Math.log(( v3 - (throttle / dragConstant)) / (v2 - (throttle / dragConstant))) / (-dragConstant));
	}
	
	public Double terminalVelocityForThrottle(Double throttle, Double dragConstant) {
		//v = h/k
		return throttle / dragConstant;
	}
	
	public Double velocityAfterTicks(Double currentVelocity, Double throttle, Double dragConstant, Double mass, Integer ticks) {
		//v(t) = (v(0) - (h/k) ) * e^ ( ( - k * t ) / m ) + ( h/k )
		return (currentVelocity - (throttle/dragConstant) ) * Math.exp(((-dragConstant * ticks )/mass )) + ( throttle/dragConstant );
	}
	
	//How many ticks do you need to get to a given speed v:
    //t = ( ln ( (v - ( h/k ) )/(v(0) - ( h/k ) ) ) * m ) / ( -k )
	//You should round this value to the next integer
	
	//Distance traveled in t ticks:
	//d(t) = ( m/k ) * ( v(0) - ( h/k ) ) * ( 1.0 - e^ ( ( -k*t ) / m ) ) + ( h/k ) * t + d(0)
	//d(0): Your current position, 0.0 if you want a relative distance
	
   //Forgot your throttle or want to know the throttle an opponent has set 
   //(you need two speed mesurements [t is the amount of ticks between them, works best if t is 1]):
   //h = ( k * ( v(t) * e^ ( ( k * t ) / m ) - v(0) ) / ( e^ ( (k * t) /m ) - 1.0 ) )
}
