package done.bot;

import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.engine.RaceEngine;
import done.model.CarPosition;
import done.model.GenericMessageWrapper;

public class NoobBot extends AbstractBot {

	private final static Logger LOGGER = LoggerFactory.getLogger(NoobBot.class);
	private Double throttle = 0.3d;
	private Random rnd = new Random();
	private boolean switchLanes = true;
	private boolean useTurbo = true;

	public NoobBot(RaceEngine raceEngine, String name, String key) {
		super(raceEngine, name, key);
	}

	public NoobBot(RaceEngine raceEngine, String name, String key, Double throttle) {
		this(raceEngine, name, key);
		this.throttle = throttle;
	}
	
	public NoobBot(RaceEngine raceEngine, String botName, String botKey,
			double throttle2, boolean useTurbo, boolean switchLanes) {
		this(raceEngine, botName, botKey, throttle2);
		this.useTurbo = useTurbo;
		this.switchLanes = switchLanes;
		
	}
 
	@Override
	void handleCarPositions(
			GenericMessageWrapper<List<CarPosition>> carPositionstMsg,
			Metrics metrics) {
		
		if ( switchLanes && gameState().canSwitch()) {
			if (rnd.nextBoolean()) {
				raceEngine().switchLeft();	
			} else {
				raceEngine().switchRight();
			}
		} else if ( useTurbo && gameState().turboAvailable() != null){
			raceEngine().turbo("Suck that!!!");	
		} else {
			raceEngine().throttle(throttle);
		}
		
		LOGGER.info("{}/{} [{}/{}] unb={}, throttle={}, velocity={} ", metrics.position.inPieceDistance()
				, metrics.piece.length()
				, metrics.position.pieceIndex()
				, gameState().race().track().pieces.size()
				, metrics.distanceToNextBend
				, throttle
				, metrics.velocity
				);
	}
}