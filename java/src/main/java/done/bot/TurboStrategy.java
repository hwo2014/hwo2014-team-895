package done.bot;

import done.model.Action;

public interface TurboStrategy {

	void apply(GameState gameState, Metrics metrics, Action action);
	
}