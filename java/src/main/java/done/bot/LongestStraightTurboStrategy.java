package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.TurboAvailable;

public class LongestStraightTurboStrategy implements TurboStrategy {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(LongestStraightTurboStrategy.class);

	@Override
	public void apply(GameState gameState, Metrics metrics, Action action) {
		TurboAvailable turboAvailable = gameState.turboAvailable();
		if (turboAvailable != null) {
			LOGGER.debug("There is a turbo available: {}", turboAvailable);
			if (!metrics.turboOn) {
				LOGGER.debug("Turbo is currently OFF");
				int currentIndex = metrics.piece.index();
				int index = gameState.track().longestStraight().first().index();
				if (currentPositionSurrounds(currentIndex, index)) {
					action.turbo = true;
				}
			} else {
				LOGGER.debug("Turbo is currently ON");
			}
		} else {
			LOGGER.debug("No turbo available.");
		}
	}

	private boolean currentPositionSurrounds(int currentIndex,
			int longestStraightInitIndex) {
		return currentIndex == (longestStraightInitIndex - 1)
				|| currentIndex == longestStraightInitIndex
				|| currentIndex == (longestStraightInitIndex + 1);
	}

}
