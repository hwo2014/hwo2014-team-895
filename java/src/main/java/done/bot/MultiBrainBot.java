package done.bot;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import done.engine.RaceEngine;
import done.model.CarId;
import done.model.GenericMessageWrapper;
import done.model.LapFinished;
import done.model.TrackPiece;

public class MultiBrainBot extends BrainyTurboBot {

	private static final TwoLongestStraightTurboStrategy TWO_LONGEST_TURBO_STRATEGY = new TwoLongestStraightTurboStrategy();
	private static final FancySwitchLaneStrategy SWITCH_LANE_STRATEGY = new FancySwitchLaneStrategy();
	private static final NoTurboStrategy NO_TURBO_STRATEGY = new NoTurboStrategy();
	private static final LongestStraightTurboStrategy LS_TURBO_STRATEGY = new LongestStraightTurboStrategy();
	private static final DistanceToBendTurboStrategy DB_TURBO_STRATEGY = new DistanceToBendTurboStrategy();

	final static Logger LOGGER = LoggerFactory.getLogger(MultiBrainBot.class);

	Map<String, BotBrain> racers = ImmutableMap.<String, BotBrain>of(
			"racer", new RacerBrain(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"racer2", new RacerBrain2(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"racer3", new RacerBrain3(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"racer7", new RacerBrain7(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"racer71", new RacerBrain71(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY)
	);
	
	Map<String, BotBrain> fuzzies = ImmutableMap.<String, BotBrain>of(
			"fuzzy3", new FuzzyBotBrain3(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"fuzzy4", new FuzzyBotBrain4(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"fuzzy5", new FuzzyBotBrain5(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"fuzzy6", new FuzzyBotBrain6(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY),
			"fuzzy7", new FuzzyBotBrain7(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY)
	);
	
	Map<String, BotBrain> brains = Maps.newHashMap();
	
	Map<String, TurboStrategy> turbos = ImmutableMap.<String, TurboStrategy>of(
			"NT", NO_TURBO_STRATEGY,
			"NS", NO_TURBO_STRATEGY,

			"LS", LS_TURBO_STRATEGY,
			"DB", DB_TURBO_STRATEGY, 
			"TL", TWO_LONGEST_TURBO_STRATEGY
	);
	
	private BotBrain brain;
	private Random random = new Random();
	
	private LinkedList<LapFinished> lapsFinished = Lists.newLinkedList(); 

	public MultiBrainBot(RaceEngine raceEngine, String name, String key, BotBrain brain) {
		super(raceEngine, name, key, brain);
		
		brains = ImmutableMap.<String, BotBrain>builder()
				.putAll(racers)
				.putAll(fuzzies)
				.build();
		
		turbos = ImmutableMap.<String, TurboStrategy>builder()
				.putAll(turbos)
				.put("1", NO_TURBO_STRATEGY)
				.put("2", NO_TURBO_STRATEGY)
				.build();
		

	}
	
	@Override
	public void onGameInit(done.model.GenericMessageWrapper<done.model.GameInit> gameInitMsg) {
		super.onGameInit(gameInitMsg);
		GameState gameState = gameState();

		String trackId = gameState.track().id;
		switch (trackId) {
			case "suzuka":
				this.brain = new FuzzyBotBrain5(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "keimola":
				this.brain = new RacerBrain7(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "elaeintarha":
				this.brain = new RacerBrain7(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "pentag":
				this.brain = new FuzzyBotBrain5(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "germany":
				this.brain = new RacerBrain2(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "usa":
				this.brain = new RacerBrain7(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "france":
				this.brain = new RacerBrain2(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "imola":
				this.brain = new FuzzyBotBrain5(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
			case "england":
				this.brain = new FuzzyBotBrain5(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;		
			default:
				this.brain = new RacerBrain7(NO_TURBO_STRATEGY, SWITCH_LANE_STRATEGY);
				break;
		}
		
		this.brain.startBrain(gameState);
	};
	
	@Override
	public void onLapFinished(done.model.GenericMessageWrapper<LapFinished> msg) {
		super.onLapFinished(msg);
		LapFinished lapFinished = msg.data();
		// se tiver mal mudar estrategia (turbo ou  throttling)
		if (lapsFinished.isEmpty()) {
			// first lap
			if (lapFinished.ranking().overall > 3) {
				pickRandomBrain(brains);
			}
		} else {
			LapFinished last = lapsFinished.getLast();
			if (last.ranking().overall < lapFinished.ranking().overall) {
				pickRandomBrain(brains);
			}
		}
		lapsFinished.add(lapFinished);
	};

	private void pickRandomBrain(Map<String, BotBrain> brains) {
		List<String> keys = Lists.newArrayList(brains.keySet());
		String randomKey = keys.get(random.nextInt(keys.size()));
		this.brain = brains.get(randomKey);
		this.brain.startBrain(gameState());
		pickRandomTurbo();
	}

	private void pickRandomTurbo() {
		List<String> keys = Lists.newArrayList(turbos.keySet());
		String randomKey = keys.get(random.nextInt(keys.size()));
		this.brain.setTurboStrategy(turbos.get(randomKey));
	}
	
	@Override
	public void onCrash(done.model.GenericMessageWrapper<done.model.CarId> carId) {
		super.onCrash(carId);
		// se tiver mal mudar estrategia (turbo ou  throttling)
		if (car.equals(carId.data())) {
			pickRandomBrain(brains);
			this.brain.setTurboStrategy(NO_TURBO_STRATEGY);
		}
	}
	
	@Override
	public void onSpawn(GenericMessageWrapper<CarId> carId) {
		super.onSpawn(carId);
		if (car.equals(carId.data())) {
			TrackPiece piece = gameState().track().piece(gameState().lastPosition());
		    pickRandomBrain(brains);
		}
	}
}
