package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.Track;
import done.model.TurboAvailable;

public class BestSpotTurboStrategy implements TurboStrategy {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(BestSpotTurboStrategy.class);

	@Override
	public void apply(GameState gameState, Metrics metrics, Action action) {
		TurboAvailable turboAvailable = gameState.turboAvailable();

		if (turboAvailable != null) {
			LOGGER.debug("There is a turbo available: {}", turboAvailable);
			if (!metrics.turboOn) {
				LOGGER.debug("Turbo is currently OFF");
				int currentIndex = metrics.piece.index();
				Track track = gameState.track();
				
				
				
				int index = track.longestStraight().first().index();
				action.turbo = true;
				
			} else {
				action.turbo = false;
			}
		} else {
			action.turbo = false;
		}

	}

}
