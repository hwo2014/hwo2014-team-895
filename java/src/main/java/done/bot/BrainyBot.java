package done.bot;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

import done.engine.RaceEngine;
import done.model.Action;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;

public class BrainyBot extends AbstractBot {

	final static Logger LOGGER = LoggerFactory.getLogger(BrainyBot.class);

	BotBrain brain;

	public BrainyBot(RaceEngine raceEngine, String name, String key, BotBrain brain) {
		super(raceEngine, name, key);
		this.brain = brain;
	}
	
	@Override
	public void onGameInit(GenericMessageWrapper<GameInit> gameInitMsg) {
		super.onGameInit(gameInitMsg);
		brain.startBrain(gameState());
	}

	@Override
	void handleCarPositions(
			GenericMessageWrapper<List<CarPosition>> carPositionstMsg,
			Metrics metrics) {
		
		Action action = brain.analyse(metrics, gameState(), raceEngine());
		metrics.throttle = action.throttle.value();
		metrics.botName = name();
		
		if (action.switchLeft) {
			raceEngine().switchLeft();
		} else if (action.switchRight) {
			raceEngine().switchRight();
		} else if (action.turbo) {
			raceEngine().turbo("VQV!");
		} else {
			raceEngine().throttle(action.throttle.value());
		}
		
		LOGGER.debug(MarkerFactory.getMarker("metrics"), "{}", metrics.toString()); 		
	}

}
