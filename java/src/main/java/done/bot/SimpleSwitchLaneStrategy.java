package done.bot;

import done.model.CarPosition;

public class SimpleSwitchLaneStrategy implements SwitchLaneStrategy {

	@Override
	public LaneCrossingAction execute(GameState gameState, CarPosition carPosition,
			boolean isLeftLaneOrFrontLeftBlocked, boolean isFrontBlocked,
			boolean isRightLaneOrFrontRightBlocked) {

		if (isFrontBlocked && gameState.canSwitch()) {
			if (!isLeftLaneOrFrontLeftBlocked) {
				return LaneCrossingAction.MOVE_LEFT;
			} else if (!isRightLaneOrFrontRightBlocked) {
				return LaneCrossingAction.MOVE_RIGHT;
			}
		}
		
		return LaneCrossingAction.DONT_MOVE;
		
	}

}
