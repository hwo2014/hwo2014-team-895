package done.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.model.Action;
import done.model.Throttle;

public class RacerBrain extends FuzzyBotBrain4 {

	public RacerBrain(TurboStrategy turboStrategy, SwitchLaneStrategy switchLaneStrategy) {
		super(turboStrategy, switchLaneStrategy);
	}

	private final static Logger LOGGER = LoggerFactory.getLogger(RacerBrain.class);
	
	Boolean topSpeedAdjusted = false;
	
	@Override
	protected void applyThrottlingStrategy(Metrics metrics, GameState gameState,
			Action action) {
		
		velocityHistory.add(metrics.velocity);
		
		if (metrics.gameTick == null || metrics.gameTick < 20) {
			
			action.throttle = new Throttle(FULL_THROTTLE);
			
		} else {
			
			adjustTopSpeed(terminalVelocityForThrottle(FULL_THROTTLE, dragConstant()));
			
			velocity.setInputValue(velocity(metrics));
			distanceToBend.setInputValue(distanceToBend(metrics));
			angle.setInputValue(Math.abs(angle(metrics)));
			
			if (gameState.isCarOnStraightPiece()) {
				engineForStraightPiece.process();
			} else {
				engineForBendPiece.process();
			}
			Double defuzzifiedThrottle = throttle.defuzzify();
			if (defuzzifiedThrottle == Double.NaN) {
				LOGGER.warn("Using default throttle.");
				defuzzifiedThrottle = throttle.getDefaultValue();
			}
			
			if (metrics.turboOn) {
				// adjust throttle ???
				//defuzzifiedThrottle = defuzzifiedThrottle *  
			}
			
			action.throttle = new Throttle(defuzzifiedThrottle);
		}
		LOGGER.debug("velocity:{}, distanceToBend:{}, angle:{} => throttle:{}", metrics.velocity, metrics.distanceToNextBend, metrics.angle, action.throttle.value());
	}

}
