var socket = io.connect('http://localhost:3000');
var graphData = [];
var graphData2 = [];
var g = new Dygraph(document.getElementById("throttle"), graphData,
      {
       width: 1024,
        height: 320,
      
        drawPoints: true,
        showRoller: true,
        valueRange: [0.0, 1.0],
        labels: ['Ticks', 'Throttle', 'Piece']
      });

var v = new Dygraph(document.getElementById("velocity"), graphData2,
      {
       width: 1024,
        height: 320,
        drawPoints: true,
        showRoller: true,
        valueRange: [0.0, 15],
        labels: ['Ticks', 'Velocity', 'Piece']
      });


socket.on('data', function (data) {
	try {
	    var received = JSON.parse(data);
	    if (received.position.id.name === "racer") {
	    	
	    	graphData.push([received.gameTick, received.throttle, received.position.piecePosition.pieceIndex/10 ]);
	    	g.updateOptions( { 
	    		'file': graphData
	    	} );	    
	    	
	    	graphData2.push([received.gameTick, received.velocity, received.position.piecePosition.pieceIndex/10 ]);
	    	v.updateOptions( { 'file': graphData2 } );
	    }
    } catch(e) {
	    console.log(e)
    }
});