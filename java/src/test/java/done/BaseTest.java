package done;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.Gson;

import done.gson.GsonFactory;
import done.model.CarId;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.PiecePosition;
import done.model.PiecePosition.Lane;
import done.model.Race;
import done.model.Track;

public class BaseTest {

	protected final Gson gson = GsonFactory.gson();

	public BaseTest() {
		super();
	}

	protected Reader json(String fileName) {
		InputStream is = getClass().getResourceAsStream(fileName);
		Reader reader = new InputStreamReader(is);
		return reader;
	}

	protected GenericMessageWrapper<GameInit> gameInit(String fileName) {
		return gson.fromJson(json(fileName), GsonFactory.genericMessageWrapperType);
	}

	protected GenericMessageWrapper<List<CarPosition>> carPositions(String fileName) {
		return gson.fromJson(json(fileName), GsonFactory.genericMessageWrapperType);
	}

	protected CarPosition position(Float angle, PiecePosition piecePosition) {
		return new CarPosition(carId(), angle, piecePosition);
	}

	protected PiecePosition piecePosition(Integer pieceIndex, Float inPieceDistance, Integer lap) {
		return new PiecePosition(pieceIndex, inPieceDistance, lane(), lap);
	}

	protected PiecePosition piecePosition(Integer pieceIndex, Float inPieceDistance, Integer lap, Integer laneStart, Integer laneEnd) {
		return new PiecePosition(pieceIndex, inPieceDistance, new Lane(laneStart, laneEnd), lap);
	}
	
	private Lane lane() {
		return new Lane(0, 0);
	}

	protected CarId carId() {
		return new CarId("testCar", "red");
	}


	protected Track trackByFileName(String fileName) {
		Race race = raceByFileName(fileName);
		return race.track();
	}

	protected Race raceByFileName(String fileName) {
		GenericMessageWrapper<GameInit> gameInitMsg = gameInit(fileName);
		Race race = gameInitMsg.data().race();
		return race;
	}

	protected Track usa() {
		return trackByFileName("/usa_game_init.json");
	}

	protected Track england() {
		return trackByFileName("/england_game_init.json");
	}
	
	protected Track elaeintarha() {
		return trackByFileName("/elaeintarha_game_init.json");
	}
	
	protected Track suzuka() {
		return trackByFileName("/suzuka_game_init.json");
	}

	protected Track keimola() {
		return trackByFileName("/keimola_game_init.json");
	}
	
	protected Track germany() {
		return trackByFileName("/germany_game_init.json");
	}
	
	protected Track pentag() {
		return trackByFileName("/pentag_game_init.json");
	}
	
	protected Track france() {
		return trackByFileName("/france_game_init.json");
	}
	
	protected Track imola() {
		return trackByFileName("/imola_game_init.json");
	}	

	protected List<Track> tracks() {
		return Lists.newArrayList(imola(), france(), pentag(), germany(),
				keimola(), suzuka(), elaeintarha(), england(), usa());
	}
	
	protected Iterator<Track> forEachTrack() {
		return tracks().iterator();
	}
	
	
}