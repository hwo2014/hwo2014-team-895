package done.bot;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import done.BaseTest;
import done.model.CarPosition;
import done.model.Track;

public class LaneCrossingStrategyTest extends BaseTest {

	@Test
	public void shouldMoveRight() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(3, 90f, 1, 0, 0));
		SwitchLaneStrategy strategy = new FancySwitchLaneStrategy();
		LaneCrossingAction action = strategy.execute(gameState(keimola), position,true,true,false);
		assertThat(action, is(LaneCrossingAction.MOVE_RIGHT));
	}

	public GameState gameState(Track keimola) {
		GameState gs = mock(GameState.class);
		when(gs.track()).thenReturn(keimola);
		return gs;
	}
	
	@Test
	public void shouldNotMove() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(3, 90f, 1, 1, 1));
		SwitchLaneStrategy strategy = new FancySwitchLaneStrategy();
		LaneCrossingAction action = strategy.execute(gameState(keimola), position,true,true,true);
		assertThat(action, is(LaneCrossingAction.DONT_MOVE));
	}
	
	@Test
	public void shouldMoveLeft() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(9, 90f, 1, 1, 1));
		SwitchLaneStrategy strategy = new FancySwitchLaneStrategy();
		LaneCrossingAction action = strategy.execute(gameState(keimola), position,false,true,true);
		assertThat(action, is(LaneCrossingAction.MOVE_LEFT));
	}
	
	@Test
	public void shouldMoveLeftWhenInsideCurve() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(7, 90f, 1, 1, 1));
		SwitchLaneStrategy strategy = new FancySwitchLaneStrategy();
		LaneCrossingAction action = strategy.execute(gameState(keimola), position,false,true,true);
		assertThat(action, is(LaneCrossingAction.MOVE_LEFT));
	}


	
/*	
 * {"msgType":"carPositions","data":[
 * {"id":{"name":"fuzzy4","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":2,"inPieceDistance":2.660854720467583,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}},
 * {"id":{"name":"racer","color":"blue"},"angle":0.0,
 * "piecePosition":{"pieceIndex":1,"inPieceDistance":97.75456036853711,"lane":{"startLaneIndex":1,"endLaneIndex":2},"lap":0}}],"gameId":"32697e7d-847e-4ea0-81cf-0eb5cc0646a4","gameTick":65}
	{"msgType":"carPositions","data":[
	{"id":{"name":"fuzzy4","color":"red"},"angle":0.0,
	"piecePosition":{"pieceIndex":2,"inPieceDistance":6.5768618717425635,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}},
	{"id":{"name":"racer","color":"blue"},"angle":0.0,
	"piecePosition":{"pieceIndex":2,"inPieceDistance":0.24648227745116458,"lane":{"startLaneIndex":2,"endLaneIndex":2},"lap":0}}],"gameId":"32697e7d-847e-4ea0-81cf-0eb5cc0646a4","gameTick":66}
*/
	@Test
	public void bug66InUSA() {
		Track usa = usa();
		CarPosition position66 = position(0f, piecePosition(2, 0.24648227745116458f, 0, 2, 2));
		Boolean toTheRight = usa.nextCurveIsToTheRight(position66);
		assertThat(toTheRight, is(true));
	}
	
}