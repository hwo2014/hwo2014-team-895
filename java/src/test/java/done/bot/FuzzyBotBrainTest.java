package done.bot;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Test;

import done.BaseTest;
import done.engine.RaceEngine;
import done.model.Action;

public class FuzzyBotBrainTest extends BaseTest {

	//private final static Logger LOGGER = LoggerFactory.getLogger(FuzzyBotBrainTest.class);

	
	@Test
	public void test() {
		
//		BotBrain brain = new FuzzyBotBrain4();
//		Metrics metrics = new Metrics();
//		metrics.velocity = 0.5d;
//		metrics.turboOn = false;
//		metrics.distanceToNextBend = 500d;
//		
//		Action action = brain.analyse(metrics, mock(GameState.class), mock(RaceEngine.class));
//		
//		assertNotNull(action.throttle);
//		assertThat(action.throttle.value(),  is(greaterThan(0d)) );
		
		//LOGGER.info(throttle.toJson());
		
	}

	
	
	//@Test
	public void test2() {
		
		BotBrain brain = new RacerBrain3(new NoTurboStrategy(), new FancySwitchLaneStrategy());

		Metrics metrics = new Metrics();
		metrics.velocity = 0.5d;
		metrics.turboOn = false;
		metrics.distanceToNextBend = 500d;
		metrics.lastPosition = position(0f, piecePosition(1, 97.75456036853711f, 0, 1, 2));
		metrics.position = position(0f, piecePosition(2, 0.24648227745116458f, 0, 2, 2));
		
		
		Action action = brain.analyse(metrics, mock(GameState.class), mock(RaceEngine.class));
		
		assertNotNull(action.throttle);
		assertThat(action.throttle.value(),  is(greaterThan(0d)) );
		
		//LOGGER.info(throttle.toJson());
		
	}
	
}
