package done.bot;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import done.engine.JsonFileRaceEngine;
import done.engine.RaceEngine;
import done.engine.RaceEventPublisher;
import done.model.SendMsg;

public class NoobBotTest {

	private final static Logger LOGGER = LoggerFactory.getLogger(NoobBotTest.class);

	//@Test
	public void test() {
		String fileName = "/r3ntF_keimola_dump.json";
		RaceEngine raceEngine = new JsonFileRaceEngine(fileName , new RaceEventPublisher() {
			@Override
			public void publish(SendMsg msg) {
				LOGGER.debug(msg.toJson());
			}
		});
		Bot newbie = 
				new BrainyTurboBot(raceEngine , "r3ntF", "testKey",
						new RacerBrain3(
								new NoTurboStrategy(), 
								new FancySwitchLaneStrategy()));
		newbie.quickRace();
	}

	@Test
	public void turboAnalyis() {
		String fileName = "/f5lsF_pentag_turbo_analysis.json";
		RaceEngine raceEngine = new JsonFileRaceEngine(fileName , new RaceEventPublisher() {
			@Override
			public void publish(SendMsg msg) {
				//LOGGER.debug(msg.toJson());
			}
		});
		Bot newbie = 
				new MultiBrainBot(raceEngine , "f5lsF", "testKey",
						new FuzzyBotBrain5(
								new LongestStraightTurboStrategy(), 
								new FancySwitchLaneStrategy()));
		newbie.quickRace();
	}

	
}