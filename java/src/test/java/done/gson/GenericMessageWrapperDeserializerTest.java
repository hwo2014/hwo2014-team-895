package done.gson;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import done.BaseTest;
import done.model.CarPosition;
import done.model.GameInit;
import done.model.GenericMessageWrapper;
import done.model.MessageType;
import done.model.Race;

public class GenericMessageWrapperDeserializerTest extends BaseTest {

	@Test
	public void shoudDeserializeGameInitMessage() {
		String fileName = "/gameInit.json";
		GenericMessageWrapper<GameInit> gameInitMsg = gameInit(fileName);
		assertNotNull(gameInitMsg);
		assertThat(gameInitMsg.type(), is(MessageType.GAME_INIT));
		
		Race race = gameInitMsg.data().race();
		assertNotNull(race);
		assertNotNull(race.track);
		assertNotNull(race.track.pieces);
		assertThat(race.track.pieces.size(), is(3));
		assertThat(race.track.length(), is(278.53981633974485d));

	}

	@Test
	public void shoudDeserializeCarPositionsMessage() {
		String fileName = "/carPositions.json";
		GenericMessageWrapper<List<CarPosition>> carPositionsMsg = carPositions(fileName);
		assertNotNull(carPositionsMsg);
		assertThat(carPositionsMsg.type(), is(MessageType.CAR_POSITIONS));
		List<CarPosition> positions = carPositionsMsg.data();
		assertNotNull(positions);
	}

}
