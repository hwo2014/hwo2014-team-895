package done.model;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.OptionalDouble;

import org.junit.Test;

import com.google.common.collect.Lists;

import done.BaseTest;

public class TrackTest extends BaseTest {

	@Test
	public void testDistanceUntilNextBend() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(0, 50f, 1));
		assertThat(keimola.distanceUntilNextBend(position), is(350d));
	}

	@Test
	public void testDistanceUntilNextBend2() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(3, 90f, 1));
		assertThat(keimola.distanceUntilNextBend(position), is(10d));
	}

	@Test
	public void whenLastPiece() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(39, 50f, 1));
		assertThat(keimola.distanceUntilNextBend(position), is(440d));
	}

	@Test
	public void whenBeforeLastPiece() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(38, 50f, 1));
		assertThat(keimola.distanceUntilNextBend(position), is(540d));
	}
	
	@Test
	public void whenWithinBend() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(4, 50f, 1));
		TrackPiece piece = keimola.piece(position);
		assertThat(keimola.distanceUntilNextBend(position), is(piece.distanceUntil(position.piecePosition())));
	}

	@Test
	public void bendLength() {
		Track keimola = keimola();
		TrackPiece piece = keimola.piece(14);
		assertThat(piece.length(), is(greaterThan(0d)));
	}

	@Test
	public void distanceTraveledFirstPieceFirstLap() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(0, 50f, 0));
		TrackPiece piece = keimola.piece(position);
		assertThat(keimola.distanceTraveled(position), is(piece.distanceUntil(position.piecePosition())));
	}

	@Test
	public void distanceTraveledSecondPieceFirstLap() {
		Track keimola = keimola();
		CarPosition position = position(0f, piecePosition(1, 50f, 0));
		assertThat(keimola.distanceTraveled(position), is(150d));
	}

	@Test
	public void curvature() {
		List<BendPiece> all = Lists.newArrayList();
		
		for (Track t : tracks()) {
			all.addAll(t.bendPieces()); 
		}

		
		OptionalDouble min = all.stream().mapToDouble(i -> i.degreeOfCurvature()).min();
		OptionalDouble max = all.stream().mapToDouble(i -> i.degreeOfCurvature()).max();
		
		assertThat(min.getAsDouble(), is(0.5d));
		assertThat(max.getAsDouble(), is(2d));
		
	}

	public void logBendPieces(Track track) {
		System.out.println(track.name);
		for (TrackPiece	 piece : track.pieces) {
			if (piece.isBend()) {
				System.out.println(piece);
			}
		}
	}
	
	@Test
	public void distanceTraveledFirstPieceSecondLap() {
		Race race = raceByFileName("/keimola_game_init.json");
		Track keimola = race.track();
		CarPosition position = position(0f, piecePosition(0, 50f, 1));
		TrackPiece piece = keimola.piece(position);
		assertThat(keimola.distanceTraveled(position), is(race.trackLength() + piece.distanceUntil(position.piecePosition())));
	}
}