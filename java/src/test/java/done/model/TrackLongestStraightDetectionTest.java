package done.model;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import done.BaseTest;
import done.model.Track.TrackSegment;

public class TrackLongestStraightDetectionTest extends BaseTest {

	@Test
	public void rawStraightDetection() {
		// 35, 36, 37, 38, 39, 40, 0, 1, 2, 3
		List<TrackSegment> segments = keimola().rawStraights();
		assertThat(segments.size(), is(7));
		for (TrackSegment trackSegment : segments) {
			assertThat(trackSegment.isStraight(), is(true));
			for (TrackPiece p : trackSegment.innerPieces) {
				assertThat(p.isStraight(), is(true));
			}
		}
	}

	@Test
	public void rawBendDetection() {
		List<TrackSegment> segments = keimola().rawCurves();
		print(segments);
		assertThat(segments.size(), is(6));
		for (TrackSegment trackSegment : segments) {
			assertThat(trackSegment.isBend(), is(true));
			System.out.println(trackSegment.averageCurvature());
			for (TrackPiece p : trackSegment.innerPieces) {
				assertThat(p.isBend(), is(true));
			}
		}
	}

	@Test
	public void segments() {
		Track track = keimola();
		List<TrackSegment> segments = track.segments();
		print(segments);
		assertThat(track.pieces.size(),
				is(segments.stream().mapToInt(i -> (int) i.size()).sum()));
		assertThat(segments.size(), is(12));
	}

	@Test
	public void optimalSegments() {
		Track track = elaeintarha();
		List<TrackSegment> segments = track.turboSpots();

		for (TrackSegment trackSegment : segments) {
			System.out.println(trackSegment.length() + "  " + trackSegment);
		}
		// assertThat(track.pieces.size(), is(segments.stream().mapToInt(i ->
		// (int) i.size()).sum()));
		// assertThat(segments.size(), is(13));
	}

	@Test
	public void straightsMerged() {
		List<TrackSegment> segments = keimola().straights();
		print(segments);

		assertThat(segments.size(), is(6));
		TrackSegment first = segments.get(0);
		assertThat(first.size(), is(9));

		assertThat(first.first().index(), is(35));
		assertThat(first.last().index(), is(3));
	}

	@Test
	public void areAdjacent() {
		Track keimola = keimola();
		List<TrackSegment> segments = keimola.rawStraights();
		print(segments);
		assertThat(
				keimola.areAdjacent(segments.get(segments.size() - 1),
						segments.get(0)), is(true));
		assertThat(
				keimola.areAdjacent(segments.get(0),
						segments.get(segments.size() - 1)), is(true));
	}

	@Test
	public void suzukaStr() {
		for (Track track : tracks()) {
			System.out.println(track.name);
			print(track.segments());
		}
	}

	public void print(List<TrackSegment> segments) {
		for (TrackSegment trackSegment : segments) {
			if (trackSegment.isBend()) {
				System.out.println(trackSegment.averageCurvature() + "  "
						+ trackSegment.length() + "  " + trackSegment);
			} else {
				System.out.println(trackSegment.length() + "  " + trackSegment);
			}
		}
	}

}
