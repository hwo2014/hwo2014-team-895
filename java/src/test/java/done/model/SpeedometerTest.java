package done.model;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import done.BaseTest;

public class SpeedometerTest extends BaseTest {

	@Test
	public void firstTickShouldReturnZero() {
		Speedometer speedometer = new Speedometer(carId(), keimola());
		speedometer.stimulus(position(0f, piecePosition(0, 15f, 0)));
		assertThat(speedometer.velocity(), is(0f));
	}
	
	@Test
	public void secondTickShouldReturnNonZero() {
		Speedometer speedometer = new Speedometer(carId(), keimola());
		speedometer.stimulus(position(0f, piecePosition(0, 15f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 30f, 0)));
		assertThat(speedometer.velocity(), is(15f));
	}

	@Test
	public void tickCrossPiecesShouldReturnPositive() {
		Track keimola = keimola();
		Speedometer speedometer = new Speedometer(carId(), keimola);
		speedometer.stimulus(position(0f, piecePosition(0, 15f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 30f, 0)));
		speedometer.stimulus(position(0f, piecePosition(1, 5f, 0)));
		assertThat(speedometer.velocity(), is(75f));
	}
	
	@Test
	public void tickCrossPiecesShouldReturnPositive2() {
		Track keimola = keimola();
		Speedometer speedometer = new Speedometer(carId(), keimola);
		speedometer.stimulus(position(0f, piecePosition(0, 15f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 30f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 50f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 75f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 100f, 0)));
		speedometer.stimulus(position(0f, piecePosition(1, 5f, 0)));
		assertThat(speedometer.velocity(), is(5f));
	}
	
	@Test
	public void tickCrossPiecesShouldReturnPositive3() {
		Track keimola = keimola();
		Speedometer speedometer = new Speedometer(carId(), keimola);
		speedometer.stimulus(position(0f, piecePosition(0, 15f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 30f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 50f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 75f, 0)));
		speedometer.stimulus(position(0f, piecePosition(0, 99f, 0)));
		speedometer.stimulus(position(0f, piecePosition(1, 5f, 0)));
		assertThat(speedometer.velocity(), is(6f));
	}

	@Test
	public void tickCrossPiecesShouldReturnPositive4() {
		Track usa = usa();
		Speedometer speedometer = new Speedometer(carId(), usa);
		speedometer.stimulus(position(0f, piecePosition(31, 75f, -1)));
		assertThat(speedometer.velocity(), is(0f));
	}

	@Test
	public void tickCrossPiecesShouldReturnPositive5() {
		Track usa = usa();
		Speedometer speedometer = new Speedometer(carId(), usa);
		speedometer.stimulus(position(0f, piecePosition(31, 75f, -1)));
		speedometer.stimulus(position(0f, piecePosition(31, 99f, -1)));
		assertThat(speedometer.velocity(), is(99f - 75f));
	}

	@Test
	public void tickCrossPiecesShouldReturnPositive6() {
		Track usa = usa();
		Speedometer speedometer = new Speedometer(carId(), usa);
		speedometer.stimulus(position(0f, piecePosition(31, 75f, -1)));
		speedometer.stimulus(position(0f, piecePosition(31, 99f, -1)));
		speedometer.stimulus(position(0f, piecePosition(0, 10f, 0)));
		assertThat(speedometer.velocity(), is(11f));
	}
	
	//@Test
	public void tickCrossBendPiecesShouldReturnPositive() {
		Track keimola = keimola();
		Speedometer speedometer = new Speedometer(carId(), keimola);
		speedometer.stimulus(position(0f, piecePosition(31, 75f, 0)));
		speedometer.stimulus(position(0f, piecePosition(31, 99f, 0)));
		speedometer.stimulus(position(0f, piecePosition(32, 1.2654f, 0)));
		assertThat(speedometer.velocity(), is(11f));
	}
}