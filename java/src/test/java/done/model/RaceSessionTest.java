package done.model;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import done.gson.GsonFactory;

public class RaceSessionTest {

	@Test
	public void qualifyingSession() {
		RaceSession session = GsonFactory.gson().fromJson("{\"durationMs\":20000}", RaceSession.class);
		assertTrue(session.isQualifying());
	}
	
	@Test
	public void raceSession() {
		RaceSession session = GsonFactory.gson().fromJson("{\"laps\":5,\"maxLapTimeMs\":60000,\"quickRace\":false}", RaceSession.class);
		assertTrue(session.isRace());
		assertFalse(session.isQualifying());
		assertThat(session.laps(), is(5));
		assertThat(session.maxLapTimeMs(), is(60000l));
		assertThat(session.quickRace(), is(false));
	}
}